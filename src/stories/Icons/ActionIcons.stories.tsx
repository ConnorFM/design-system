import React from 'react';
import { Story, Meta } from '@storybook/react/types-6-0';
import { IconProps } from './Icons.interface';
import {
  Accessibility,
  AccessibilityNew,
  Accessible,
  AccessibleForward,
  AccountBalance,
  AccountBalanceWallet,
  AccountBox,
  AccountCircle,
  AddShoppingCart,
  Alarm,
  AlarmAdd,
  AlarmOff,
  AlarmOn,
  AllInbox,
  AllOut,
  Android,
  Announcement,
  ArrowRightAlt,
  AspectRatio,
  Assessment,
  Assignment,
  AssignmentInd,
  AssignmentLate,
  AssignmentReturn,
  AssignmentReturned,
  AssignmentTurnedIn,
  Autorenew,
  Backup,
  Book,
  Bookmark,
  BookmarkBorder,
  Bookmarks,
  BugReport,
  Build,
  Cached,
  CalendarToday,
  CalendarViewDay,
  CameraEnhance,
  CardGiftcard,
  CardMembership,
  CardTravel,
  ChangeHistory,
  CheckCircle,
  CheckCircleOutline,
  ChromeReaderMode,
  ClassIcon,
  Code,
  Commute,
  CompareArrows,
  ContactSupport,
  Copyright,
  CreditCard,
  Dashboard,
  DateRange,
  Delete,
  DeleteForever,
  DeleteOutline,
  Description,
  Dns,
  Done,
  DoneAll,
  DoneOutline,
  DonutLarge,
  DonutSmall,
  DragIndicator,
  Eject,
  EuroSymbol,
  Event,
  EventSeat,
  ExitToApp,
  Explore,
  ExploreOff,
  Extension,
  Face,
  FaceUnlock,
  Favorite,
  FavoriteBorder,
  Feedback,
  FindInPage,
  FindReplace,
  Fingerprint,
  FlightLand,
  FlightTakeoff,
  FlipToBack,
  FlipToFront,
  GTranslate,
  Gavel,
  GetApp,
  Gif,
  Grade,
  GroupWork,
  Help,
  HelpOutline,
  HighlightOff,
  History,
  Home,
  HorizontalSplit,
  HourglassEmpty,
  HourglassFull,
  Http,
  Https,
  ImportantDevices,
  Info,
  InfoOutline,
  InputIcon,
  InvertColors,
  LabelIcon,
  LabelImportant,
  LabelImportantOutline,
  LabelOff,
  LabelOutline,
  Language,
  Launch,
  LightbulbOutline,
  LineStyle,
  LineWeight,
  List,
  Lock,
  LockOpen,
  LockOutline,
  Loyalty,
  MarkunreadMailbox,
  Maximize,
  Minimize,
  Motorcycle,
  NoteAdd,
  OfflineBolt,
  OfflinePin,
  Opacity,
  OpenInBrowser,
  OpenInNew,
  OpenWith,
  Pageview,
  PanTool,
  Payment,
  PermCameraMic,
  PermContactCalendar,
  PermDataSetting,
  PermDeviceInformation,
  PermIdentity,
  PermMedia,
  PermPhoneMsg,
  PermScanWifi,
  Pets,
  PictureInPicture,
  PictureInPictureAlt,
  PlayForWork,
  Polymer,
  PowerSettingsNew,
  PregnantWoman,
  Print,
  QueryBuilder,
  QuestionAnswer,
  Receipt,
  RecordVoiceOver,
  Redeem,
  RemoveShoppingCart,
  Reorder,
  ReportProblem,
  Restore,
  RestoreFromTrash,
  RestorePage,
  Room,
  RoundedCorner,
  Rowing,
  Schedule,
  Search,
  Settings,
  SettingsApplications,
  SettingsBackupRestore,
  SettingsBluetooth,
  SettingsBrightness,
  SettingsCell,
  SettingsEthernet,
  SettingsInputAntenna,
  SettingsInputComponent,
  SettingsInputComposite,
  SettingsInputHdmi,
  SettingsInputSvideo,
  SettingsOverscan,
  SettingsPhone,
  SettingsPower,
  SettingsRemote,
  SettingsVoice,
  Shop,
  ShopTwo,
  ShoppingBasket,
  ShoppingCart,
  SpeakerNotes,
  SpeakerNotesOff,
  Spellcheck,
  StarRate,
  Stars,
  Store,
  Subject,
  SupervisedUserCircle,
  SupervisorAccount,
  SwapHoriz,
  SwapHorizontalCircle,
  SwapVert,
  SwapVerticalCircle,
  TabUnselected,
  TextRotateUp,
  TextRotateVertical,
  TextRotationAngleDown,
  TextRotationAngleUp,
  TextRotationDown,
  TextRotationNone,
  Theaters,
  ThreeDRotation,
  ThumbDown,
  ThumbUp,
  ThumbsUpDown,
  Today,
  Toll,
  TouchApp,
  TrackChanges,
  Translate,
  TrendingDown,
  TrendingFlat,
  TrendingUp,
  TurnedIn,
  TurnedInNot,
  Update,
  VerifiedUser,
  VerticalSplit,
  ViewAgenda,
  ViewArray,
  ViewCarousel,
  ViewColumn,
  ViewDay,
  ViewHeadline,
  ViewList,
  ViewModule,
  ViewQuilt,
  ViewStream,
  ViewWeek,
  Visibility,
  VisibilityOff,
  VoiceOverOff,
  WatchLater,
  Work,
  WorkOff,
  WorkOutline,
  YoutubeSearchedFor,
  ZoomIn,
  ZoomOut
} from '@connorfm/amiltone-icons-action'

export default {
  title: 'Icons/ActionIcons',
  component: Accessibility,
  AccessibilityNew,
  Accessible,
  AccessibleForward,
  AccountBalance,
  AccountBalanceWallet,
  AccountBox,
  AccountCircle,
  AddShoppingCart,
  Alarm,
  AlarmAdd,
  AlarmOff,
  AlarmOn,
  AllInbox,
  AllOut,
  Android,
  Announcement,
  ArrowRightAlt,
  AspectRatio,
  Assessment,
  Assignment,
  AssignmentInd,
  AssignmentLate,
  AssignmentReturn,
  AssignmentReturned,
  AssignmentTurnedIn,
  Autorenew,
  Backup,
  Book,
  Bookmark,
  BookmarkBorder,
  Bookmarks,
  BugReport,
  Build,
  Cached,
  CalendarToday,
  CalendarViewDay,
  CameraEnhance,
  CardGiftcard,
  CardMembership,
  CardTravel,
  ChangeHistory,
  CheckCircle,
  CheckCircleOutline,
  ChromeReaderMode,
  ClassIcon,
  Code,
  Commute,
  CompareArrows,
  ContactSupport,
  Copyright,
  CreditCard,
  Dashboard,
  DateRange,
  Delete,
  DeleteForever,
  DeleteOutline,
  Description,
  Dns,
  Done,
  DoneAll,
  DoneOutline,
  DonutLarge,
  DonutSmall,
  DragIndicator,
  Eject,
  EuroSymbol,
  Event,
  EventSeat,
  ExitToApp,
  Explore,
  ExploreOff,
  Extension,
  Face,
  FaceUnlock,
  Favorite,
  FavoriteBorder,
  Feedback,
  FindInPage,
  FindReplace,
  Fingerprint,
  FlightLand,
  FlightTakeoff,
  FlipToBack,
  FlipToFront,
  GTranslate,
  Gavel,
  GetApp,
  Gif,
  Grade,
  GroupWork,
  Help,
  HelpOutline,
  HighlightOff,
  History,
  Home,
  HorizontalSplit,
  HourglassEmpty,
  HourglassFull,
  Http,
  Https,
  ImportantDevices,
  Info,
  InfoOutline,
  InputIcon,
  InvertColors,
  LabelIcon,
  LabelImportant,
  LabelImportantOutline,
  LabelOff,
  LabelOutline,
  Language,
  Launch,
  LightbulbOutline,
  LineStyle,
  LineWeight,
  List,
  Lock,
  LockOpen,
  LockOutline,
  Loyalty,
  MarkunreadMailbox,
  Maximize,
  Minimize,
  Motorcycle,
  NoteAdd,
  OfflineBolt,
  OfflinePin,
  Opacity,
  OpenInBrowser,
  OpenInNew,
  OpenWith,
  Pageview,
  PanTool,
  Payment,
  PermCameraMic,
  PermContactCalendar,
  PermDataSetting,
  PermDeviceInformation,
  PermIdentity,
  PermMedia,
  PermPhoneMsg,
  PermScanWifi,
  Pets,
  PictureInPicture,
  PictureInPictureAlt,
  PlayForWork,
  Polymer,
  PowerSettingsNew,
  PregnantWoman,
  Print,
  QueryBuilder,
  QuestionAnswer,
  Receipt,
  RecordVoiceOver,
  Redeem,
  RemoveShoppingCart,
  Reorder,
  ReportProblem,
  Restore,
  RestoreFromTrash,
  RestorePage,
  Room,
  RoundedCorner,
  Rowing,
  Schedule,
  Search,
  Settings,
  SettingsApplications,
  SettingsBackupRestore,
  SettingsBluetooth,
  SettingsBrightness,
  SettingsCell,
  SettingsEthernet,
  SettingsInputAntenna,
  SettingsInputComponent,
  SettingsInputComposite,
  SettingsInputHdmi,
  SettingsInputSvideo,
  SettingsOverscan,
  SettingsPhone,
  SettingsPower,
  SettingsRemote,
  SettingsVoice,
  Shop,
  ShopTwo,
  ShoppingBasket,
  ShoppingCart,
  SpeakerNotes,
  SpeakerNotesOff,
  Spellcheck,
  StarRate,
  Stars,
  Store,
  Subject,
  SupervisedUserCircle,
  SupervisorAccount,
  SwapHoriz,
  SwapHorizontalCircle,
  SwapVert,
  SwapVerticalCircle,
  TabUnselected,
  TextRotateUp,
  TextRotateVertical,
  TextRotationAngleDown,
  TextRotationAngleUp,
  TextRotationDown,
  TextRotationNone,
  Theaters,
  ThreeDRotation,
  ThumbDown,
  ThumbUp,
  ThumbsUpDown,
  Today,
  Toll,
  TouchApp,
  TrackChanges,
  Translate,
  TrendingDown,
  TrendingFlat,
  TrendingUp,
  TurnedIn,
  TurnedInNot,
  Update,
  VerifiedUser,
  VerticalSplit,
  ViewAgenda,
  ViewArray,
  ViewCarousel,
  ViewColumn,
  ViewDay,
  ViewHeadline,
  ViewList,
  ViewModule,
  ViewQuilt,
  ViewStream,
  ViewWeek,
  Visibility,
  VisibilityOff,
  VoiceOverOff,
  WatchLater,
  Work,
  WorkOff,
  WorkOutline,
  YoutubeSearchedFor,
  ZoomIn,
  ZoomOut,
} as Meta;



const TemplateA1: Story<IconProps> = (args) =>
  <>
    <Accessibility {...args} />
    <AccessibilityNew {...args} />
    <Accessible {...args} />
    <Accessibility {...args} />
    <AccessibilityNew {...args} />
    <Accessible {...args} />
    <AccessibleForward {...args} />
    <AccountBalance {...args} />
    <AccountBalanceWallet {...args} />
    <AccountBox {...args} />
    <AccountCircle {...args} />
    <AddShoppingCart {...args} />
  </>;
const TemplateA2: Story<IconProps> = (args) =>
  <>
    <Alarm {...args} />
    <AlarmAdd {...args} />
    <AlarmOff {...args} />
    <AlarmOn {...args} />
    <AllInbox {...args} />
    <AllOut {...args} />
    <Android {...args} />
    <Announcement {...args} />
    <ArrowRightAlt {...args} />
    <AspectRatio {...args} />
    <Assessment {...args} />
    <Assignment {...args} />
    <AssignmentInd {...args} />
    <AssignmentLate {...args} />
    <AssignmentReturn {...args} />
    <AssignmentReturned {...args} />
    <AssignmentTurnedIn {...args} />
    <Autorenew {...args} />
  </>;
const TemplateB: Story<IconProps> = (args) =>
  <>
    <Backup {...args} />
    <Book {...args} />
    <Bookmark {...args} />
    <BookmarkBorder {...args} />
    <Bookmarks {...args} />
    <BugReport {...args} />
    <Build {...args} />
  </>;

const TemplateC: Story<IconProps> = (args) =>
  <>
    <Cached {...args} />
    <CalendarToday {...args} />
    <CalendarViewDay {...args} />
    <CameraEnhance {...args} />
    <CardGiftcard {...args} />
    <CardMembership {...args} />
    <CardTravel {...args} />
    <ChangeHistory {...args} />
    <CheckCircle {...args} />
    <CheckCircleOutline {...args} />
    <ChromeReaderMode {...args} />
    <ClassIcon {...args} />
    <Code {...args} />
    <Commute {...args} />
    <CompareArrows {...args} />
    <ContactSupport {...args} />
    <Copyright {...args} />
    <CreditCard {...args} />
  </>;
const TemplateD: Story<IconProps> = (args) =>
  <>
    <Dashboard {...args} />
    <DateRange {...args} />
    <Delete {...args} />
    <DeleteForever {...args} />
    <DeleteOutline {...args} />
    <Description {...args} />
    <Dns {...args} />
    <Done {...args} />
    <DoneAll {...args} />
    <DoneOutline {...args} />
    <DonutLarge {...args} />
    <DonutSmall {...args} />
    <DragIndicator {...args} />
  </>;

const TemplateE: Story<IconProps> = (args) =>
  <>
    <Eject {...args} />
    <EuroSymbol {...args} />
    <Event {...args} />
    <EventSeat {...args} />
    <ExitToApp {...args} />
    <Explore {...args} />
    <ExploreOff {...args} />
    <Extension {...args} />
  </>;

const TemplateF: Story<IconProps> = (args) =>
  <>
    <Face {...args} />
    <FaceUnlock {...args} />
    <Favorite {...args} />
    <FavoriteBorder {...args} />
    <Feedback {...args} />
    <FindInPage {...args} />
    <FindReplace {...args} />
    <Fingerprint {...args} />
    <FlightLand {...args} />
    <FlightTakeoff {...args} />
    <FlipToBack {...args} />
    <FlipToFront {...args} />
  </>;

const TemplateG: Story<IconProps> = (args) =>
  <>
    <GTranslate {...args} />
    <Gavel {...args} />
    <GetApp {...args} />
    <Gif {...args} />
    <Grade {...args} />
    <GroupWork {...args} />
  </>;

const TemplateH: Story<IconProps> = (args) =>
  <>
    <Help {...args} />
    <HelpOutline {...args} />
    <HighlightOff {...args} />
    <History {...args} />
    <Home {...args} />
    <HorizontalSplit {...args} />
    <HourglassEmpty {...args} />
    <HourglassFull {...args} />
    <Http {...args} />
    <Https {...args} />
  </>;

const TemplateI: Story<IconProps> = (args) =>
  <>
    <ImportantDevices {...args} />
    <Info {...args} />
    <InfoOutline {...args} />
    <InputIcon {...args} />
    <InvertColors {...args} />
  </>

const TemplateL: Story<IconProps> = (args) =>
  <>
    <LabelIcon {...args} />
    <LabelImportant {...args} />
    <LabelImportantOutline {...args} />
    <LabelOff {...args} />
    <LabelOutline {...args} />
    <Language {...args} />
    <Launch {...args} />
    <LightbulbOutline {...args} />
    <LineStyle {...args} />
    <LineWeight {...args} />
    <List {...args} />
    <Lock {...args} />
    <LockOpen {...args} />
    <LockOutline {...args} />
    <Loyalty {...args} />
  </>;

const TemplateM: Story<IconProps> = (args) =>
  <>
    <MarkunreadMailbox {...args} />
    <Maximize {...args} />
    <Minimize {...args} />
    <Motorcycle {...args} />
  </>;

const TemplateN: Story<IconProps> = (args) =>
  <>
    <NoteAdd {...args} />
  </>;

const TemplateO: Story<IconProps> = (args) =>
  <>
    <OfflineBolt {...args} />
    <OfflinePin {...args} />
    <Opacity {...args} />
    <OpenInBrowser {...args} />
    <OpenInNew {...args} />
    <OpenWith {...args} />
  </>

const TemplateP: Story<IconProps> = (args) =>
  <>
    <Pageview {...args} />
    <PanTool {...args} />
    <Payment {...args} />
    <PermCameraMic {...args} />
    <PermContactCalendar {...args} />
    <PermDataSetting {...args} />
    <PermDeviceInformation {...args} />
    <PermIdentity {...args} />
    <PermMedia {...args} />
    <PermPhoneMsg {...args} />
    <PermScanWifi {...args} />
    <Pets {...args} />
    <PictureInPicture {...args} />
    <PictureInPictureAlt {...args} />
    <PlayForWork {...args} />
    <Polymer {...args} />
    <PowerSettingsNew {...args} />
    <PregnantWoman {...args} />
    <Print {...args} />
  </>

const TemplateQ: Story<IconProps> = (args) =>
  <>
    <QueryBuilder {...args} />
    <QuestionAnswer {...args} />
  </>;

const TemplateR: Story<IconProps> = (args) =>
  <>
    <Receipt {...args} />
    <RecordVoiceOver {...args} />
    <Redeem {...args} />
    <RemoveShoppingCart {...args} />
    <Reorder {...args} />
    <ReportProblem {...args} />
    <Restore {...args} />
    <RestoreFromTrash {...args} />
    <RestorePage {...args} />
    <Room {...args} />
    <RoundedCorner {...args} />
    <Rowing {...args} />
  </>;

const TemplateS: Story<IconProps> = (args) =>
  <>
    <Schedule {...args} />
    <Search {...args} />
    <Settings {...args} />
    <SettingsApplications {...args} />
    <SettingsBackupRestore {...args} />
    <SettingsBluetooth {...args} />
    <SettingsBrightness {...args} />
    <SettingsCell {...args} />
    <SettingsEthernet {...args} />
    <SettingsInputAntenna {...args} />
    <SettingsInputComponent {...args} />
    <SettingsInputComposite {...args} />
    <SettingsInputHdmi {...args} />
    <SettingsInputSvideo {...args} />
    <SettingsOverscan {...args} />
    <SettingsPhone {...args} />
    <SettingsPower {...args} />
    <SettingsRemote {...args} />
    <SettingsVoice {...args} />
    <Shop {...args} />
    <ShopTwo {...args} />
    <ShoppingBasket {...args} />
    <ShoppingCart {...args} />
    <SpeakerNotes {...args} />
    <SpeakerNotesOff {...args} />
    <Spellcheck {...args} />
    <StarRate {...args} />
    <Stars {...args} />
    <Store {...args} />
    <Subject {...args} />
    <SupervisedUserCircle {...args} />
    <SupervisorAccount {...args} />
    <SwapHoriz {...args} />
    <SwapHorizontalCircle {...args} />
    <SwapVert {...args} />
    <SwapVerticalCircle {...args} />
  </>;
const TemplateT: Story<IconProps> = (args) =>
  <>
    <TabUnselected {...args} />
    <TextRotateUp {...args} />
    <TextRotateVertical {...args} />
    <TextRotationAngleDown {...args} />
    <TextRotationAngleUp {...args} />
    <TextRotationDown {...args} />
    <TextRotationNone {...args} />
    <Theaters {...args} />
    <ThreeDRotation {...args} />
    <ThumbDown {...args} />
    <ThumbUp {...args} />
    <ThumbsUpDown {...args} />
    <Today {...args} />
    <Toll {...args} />
    <TouchApp {...args} />
    <TrackChanges {...args} />
    <Translate {...args} />
    <TrendingDown {...args} />
    <TrendingFlat {...args} />
    <TrendingUp {...args} />
    <TurnedIn {...args} />
    <TurnedInNot {...args} />
  </>;
const TemplateU: Story<IconProps> = (args) =>
  <Update {...args} />;

const TemplateV: Story<IconProps> = (args) =>
  <>
    <VerifiedUser {...args} />
    <VerticalSplit {...args} />
    <ViewAgenda {...args} />
    <ViewArray {...args} />
    <ViewCarousel {...args} />
    <ViewColumn {...args} />
    <ViewDay {...args} />
    <ViewHeadline {...args} />
    <ViewList {...args} />
    <ViewModule {...args} />
    <ViewQuilt {...args} />
    <ViewStream {...args} />
    <ViewWeek {...args} />
    <Visibility {...args} />
    <VisibilityOff {...args} />
    <VoiceOverOff {...args} />
  </>;

const TemplateW: Story<IconProps> = (args) =>
  <>
    <WatchLater {...args} />
    <Work {...args} />
    <WorkOff {...args} />
    <WorkOutline {...args} />
  </>;

const TemplateY: Story<IconProps> = (args) =>
  <YoutubeSearchedFor {...args} />;

const TemplateZ: Story<IconProps> = (args) =>
  <>
    <ZoomIn {...args} />
    <ZoomOut {...args} />
  </>;


export const ActionIconsA1 = TemplateA1.bind({});
ActionIconsA1.args = {
  fillColor: 'red',
};
export const ActionIconsA2 = TemplateA2.bind({});
ActionIconsA2.args = {
  fillColor: 'red',
};
export const ActionIconsB = TemplateB.bind({});
ActionIconsB.args = {
  fillColor: 'red',
};
export const ActionIconsC = TemplateC.bind({});
ActionIconsC.args = {
  fillColor: 'red',
};
export const ActionIconsD = TemplateD.bind({});
ActionIconsD.args = {
  fillColor: 'red',
};
export const ActionIconsE = TemplateE.bind({});
ActionIconsE.args = {
  fillColor: 'red',
};
export const ActionIconsF = TemplateF.bind({});
ActionIconsF.args = {
  fillColor: 'red',
};
export const ActionIconsG = TemplateG.bind({});
ActionIconsG.args = {
  fillColor: 'red',
};
export const ActionIconsH = TemplateH.bind({});
ActionIconsH.args = {
  fillColor: 'red',
};
export const ActionIconsI = TemplateI.bind({});
ActionIconsI.args = {
  fillColor: 'red',
};
export const ActionIconsL = TemplateL.bind({});
ActionIconsL.args = {
  fillColor: 'red',
};
export const ActionIconsM = TemplateM.bind({});
ActionIconsM.args = {
  fillColor: 'red',
};
export const ActionIconsN = TemplateN.bind({});
ActionIconsN.args = {
  fillColor: 'red',
};
export const ActionIconsO = TemplateO.bind({});
ActionIconsO.args = {
  fillColor: 'red',
};
export const ActionIconsP = TemplateP.bind({});
ActionIconsP.args = {
  fillColor: 'red',
};
export const ActionIconsQ = TemplateQ.bind({});
ActionIconsQ.args = {
  fillColor: 'red',
};
export const ActionIconsR = TemplateR.bind({});
ActionIconsR.args = {
  fillColor: 'red',
};
export const ActionIconsS = TemplateS.bind({});
ActionIconsS.args = {
  fillColor: 'red',
};
export const ActionIconsT = TemplateT.bind({});
ActionIconsT.args = {
  fillColor: 'red',
};
export const ActionIconsU = TemplateU.bind({});
ActionIconsU.args = {
  fillColor: 'red',
};
export const ActionIconsV = TemplateV.bind({});
ActionIconsV.args = {
  fillColor: 'red',
};
export const ActionIconsW = TemplateW.bind({});
ActionIconsW.args = {
  fillColor: 'red',
};
export const ActionIconsY = TemplateY.bind({});
ActionIconsY.args = {
  fillColor: 'red',
};
export const ActionIconsZ = TemplateZ.bind({});
ActionIconsZ.args = {
  fillColor: 'red',
};


