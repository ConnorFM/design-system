import React from 'react';
import { Story, Meta } from '@storybook/react/types-6-0';
import { IconProps } from './Icons.interface';
import {
  CheckBox,
  CheckBoxOutlineBlank,
  IndeterminateCheckBox,
  RadioButtonChecked,
  RadioButtonUnchecked,
  Star,
  StarBorder,
  StarBorderPurple500,
  StarHalf,
  StarOutline,
  StarPurple500,
  ToggleOff,
  ToggleOn
} from '@connorfm/amiltone-icons-toggle'



export default {
  title: 'Icons/ToggleIcons',
  component: CheckBox,
  CheckBoxOutlineBlank,
  IndeterminateCheckBox,
  RadioButtonChecked,
  RadioButtonUnchecked,
  Star,
  StarBorder,
  StarBorderPurple500,
  StarHalf,
  StarOutline,
  StarPurple500,
  ToggleOff,
  ToggleOn
} as Meta;



const Template: Story<IconProps> = (args) =>
  <>
    <CheckBox {...args} />
    <CheckBoxOutlineBlank {...args} />
    <IndeterminateCheckBox {...args} />
    <RadioButtonChecked {...args} />
    <RadioButtonUnchecked {...args} />
    <Star {...args} />
    <StarBorder {...args} />
    <StarBorderPurple500 {...args} />
    <StarHalf {...args} />
    <StarOutline {...args} />
    <StarPurple500 {...args} />
    <ToggleOff {...args} />
    <ToggleOn {...args} />
  </>

export const ToggleIcons = Template.bind({});
ToggleIcons.args = {
  fillColor: 'red',
};
