import React from 'react';
import { Story, Meta } from '@storybook/react/types-6-0';
import { IconProps } from './Icons.interface';
import {
  Add,
  Error,
  ErrorOutline,
  NotificationImportant,
  Warning,
  WarningAmber,
} from '@connorfm/amiltone-icons-alert'

export default {
  title: 'Icons/AlertIcons',
  component: Add,
  Error,
  ErrorOutline,
  NotificationImportant,
  Warning,
  WarningAmber,
} as Meta;



const Template: Story<IconProps> = (args) =>
  <>
    <Add {...args} />
    <Error {...args} />
    <ErrorOutline {...args} />
    <NotificationImportant {...args} />
    <Warning {...args} />
    <WarningAmber {...args} />
  </>;


export const AlertIcons = Template.bind({});
AlertIcons.args = {
  fillColor: 'red',
};



