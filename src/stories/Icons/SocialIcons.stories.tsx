import React from 'react';
import { Story, Meta } from '@storybook/react/types-6-0';
import { IconProps } from './Icons.interface';
import {
  Cake,
  Domain,
  Group,
  GroupAdd,
  LocationCity,
  Mood,
  MoodBad,
  Notifications,
  NotificationsActive,
  NotificationsNone,
  NotificationsOff,
  NotificationsPaused,
  Pages,
  PartyMode,
  People,
  PeopleOutline,
  Person,
  PersonAdd,
  PersonOutline,
  PlusOne,
  Poll,
  Publicon,
  School,
  SentimentDissatisfied,
  SentimentNeutral,
  SentimentSatisfied,
  SentimentVeryDissatisfied,
  SentimentVerySatisfied,
  Share,
  ThumbDownAlt,
  ThumbUpAlt,
  Whatshot
} from '@connorfm/amiltone-icons-social'



export default {
  title: 'Icons/SocialIcons',
  component: Cake,
  Domain,
  Group,
  GroupAdd,
  LocationCity,
  Mood,
  MoodBad,
  Notifications,
  NotificationsActive,
  NotificationsNone,
  NotificationsOff,
  NotificationsPaused,
  Pages,
  PartyMode,
  People,
  PeopleOutline,
  Person,
  PersonAdd,
  PersonOutline,
  PlusOne,
  Poll,
  Publicon,
  School,
  SentimentDissatisfied,
  SentimentNeutral,
  SentimentSatisfied,
  SentimentVeryDissatisfied,
  SentimentVerySatisfied,
  Share,
  ThumbDownAlt,
  ThumbUpAlt,
  Whatshot
} as Meta;



const TemplateAN: Story<IconProps> = (args) =>
  <>
    <Cake {...args} />
    <Domain {...args} />
    <Group {...args} />
    <GroupAdd {...args} />
    <LocationCity {...args} />
    <Mood {...args} />
    <MoodBad {...args} />
    <Notifications {...args} />
    <NotificationsActive {...args} />
    <NotificationsNone {...args} />
    <NotificationsOff {...args} />
    <NotificationsPaused {...args} />
  </>

const TemplatePW: Story<IconProps> = (args) =>
  <>
    <Pages {...args} />
    <PartyMode {...args} />
    <People {...args} />
    <PeopleOutline {...args} />
    <Person {...args} />
    <PersonAdd {...args} />
    <PersonOutline {...args} />
    <PlusOne {...args} />
    <Poll {...args} />
    <Publicon {...args} />
    <School {...args} />
    <SentimentDissatisfied {...args} />
    <SentimentNeutral {...args} />
    <SentimentSatisfied {...args} />
    <SentimentVeryDissatisfied {...args} />
    <SentimentVerySatisfied {...args} />
    <Share {...args} />
    <ThumbDownAlt {...args} />
    <ThumbUpAlt {...args} />
    <Whatshot {...args} />
  </>

export const SocialIconsAN = TemplateAN.bind({});
SocialIconsAN.args = {
  fillColor: 'red',
};
export const SocialIconsPW = TemplatePW.bind({});
SocialIconsPW.args = {
  fillColor: 'red',
};
