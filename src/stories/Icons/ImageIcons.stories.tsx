import React from 'react';
import { Story, Meta } from '@storybook/react/types-6-0';
import { IconProps } from './Icons.interface';
import {
  AddAPhoto,
  AddPhotoAlternate,
  AddToPhotos,
  Adjust,
  Assistant,
  AssistantPhoto,
  Audiotrack,
  BlurCircular,
  BlurLinear,
  BlurOff,
  BlurOn,
  Brightness1,
  Brightness2,
  Brightness3,
  Brightness4,
  Brightness5,
  Brightness6,
  Brightness7,
  BrokenImage,
  Brush,
  BurstMode,
  Camera,
  CameraAlt,
  CameraFront,
  CameraRear,
  CameraRoll,
  CenterFocusStrong,
  CenterFocusWeak,
  Collections,
  CollectionsBookmark,
  ColorLens,
  Colorize,
  Compare,
  ControlPoint,
  ControlPointDuplicate,
  Crop,
  CropDin,
  CropFree,
  CropLandscape,
  CropOriginal,
  CropPortrait,
  CropRotate,
  CropSquare,
  Crop169,
  Crop32,
  Crop54,
  Crop75,
  Dehaze,
  Details,
  Edit,
  Exposure,
  ExposureNeg1,
  ExposureNeg2,
  ExposurePlus1,
  ExposurePlus2,
  ExposureZero,
  Filter,
  FilterBAndW,
  FilterCenterFocus,
  FilterDrama,
  FilterFrames,
  FilterHdr,
  FilterNone,
  FilterTiltShift,
  FilterVintage,
  Filter1,
  Filter2,
  Filter3,
  Filter4,
  Filter5,
  Filter6,
  Filter7,
  Filter8,
  Filter9,
  Filter9Plus,
  Flare,
  FlashAuto,
  FlashOff,
  FlashOn,
  Flip,
  Gradient,
  Grain,
  GridOff,
  GridOn,
  HdrOff,
  HdrOn,
  HdrStrong,
  HdrWeak,
  Healing,
  Image,
  ImageAspectRatio,
  ImageSearch,
  Iso,
  Landscape,
  LeakAdd,
  LeakRemove,
  Lens,
  LinkedCamera,
  Looks,
  LooksOne,
  LooksTwo,
  Looks3,
  Looks4,
  Looks5,
  Looks6,
  Loupe,
  MonochromePhotos,
  MovieCreation,
  MovieFilter,
  MusicNote,
  MusicOff,
  Nature,
  NaturePeople,
  NavigateBefore,
  NavigateNext,
  Palette,
  Panorama,
  PanoramaFishEye,
  PanoramaHorizontal,
  PanoramaVertical,
  PanoramaWideAngle,
  Photo,
  PhotoAlbum,
  PhotoCamera,
  PhotoFilter,
  PhotoLibrary,
  PhotoSizeSelectActual,
  PhotoSizeSelectLarge,
  PhotoSizeSelectSmall,
  PictureAsPdf,
  Portrait,
  RemoveRedEye,
  RotateLeft,
  RotateRight,
  Rotate90DegreesCcw,
  ShutterSpeed,
  Slideshow,
  Straighten,
  Style,
  SwitchCamera,
  SwitchVideo,
  TagFaces,
  Texture,
  Timelapse,
  Timer,
  TimerOff,
  Timer10,
  Timer3,
  Tonality,
  Transform,
  Tune,
  ViewComfy,
  ViewCompact,
  Vignette,
  WbAuto,
  WbCloudy,
  WbIncandescent,
  WbIridescent,
  WbSunny
} from '@connorfm/amiltone-icons-image'



export default {
  title: 'Icons/ImageIcons',
  component: AddAPhoto,
  AddPhotoAlternate,
  AddToPhotos,
  Adjust,
  Assistant,
  AssistantPhoto,
  Audiotrack,
  BlurCircular,
  BlurLinear,
  BlurOff,
  BlurOn,
  Brightness1,
  Brightness2,
  Brightness3,
  Brightness4,
  Brightness5,
  Brightness6,
  Brightness7,
  BrokenImage,
  Brush,
  BurstMode,
  Camera,
  CameraAlt,
  CameraFront,
  CameraRear,
  CameraRoll,
  CenterFocusStrong,
  CenterFocusWeak,
  Collections,
  CollectionsBookmark,
  ColorLens,
  Colorize,
  Compare,
  ControlPoint,
  ControlPointDuplicate,
  Crop,
  CropDin,
  CropFree,
  CropLandscape,
  CropOriginal,
  CropPortrait,
  CropRotate,
  CropSquare,
  Crop169,
  Crop32,
  Crop54,
  Crop75,
  Dehaze,
  Details,
  Edit,
  Exposure,
  ExposureNeg1,
  ExposureNeg2,
  ExposurePlus1,
  ExposurePlus2,
  ExposureZero,
  Filter,
  FilterBAndW,
  FilterCenterFocus,
  FilterDrama,
  FilterFrames,
  FilterHdr,
  FilterNone,
  FilterTiltShift,
  FilterVintage,
  Filter1,
  Filter2,
  Filter3,
  Filter4,
  Filter5,
  Filter6,
  Filter7,
  Filter8,
  Filter9,
  Filter9Plus,
  Flare,
  FlashAuto,
  FlashOff,
  FlashOn,
  Flip,
  Gradient,
  Grain,
  GridOff,
  GridOn,
  HdrOff,
  HdrOn,
  HdrStrong,
  HdrWeak,
  Healing,
  Image,
  ImageAspectRatio,
  ImageSearch,
  Iso,
  Landscape,
  LeakAdd,
  LeakRemove,
  Lens,
  LinkedCamera,
  Looks,
  LooksOne,
  LooksTwo,
  Looks3,
  Looks4,
  Looks5,
  Looks6,
  Loupe,
  MonochromePhotos,
  MovieCreation,
  MovieFilter,
  MusicNote,
  MusicOff,
  Nature,
  NaturePeople,
  NavigateBefore,
  NavigateNext,
  Palette,
  Panorama,
  PanoramaFishEye,
  PanoramaHorizontal,
  PanoramaVertical,
  PanoramaWideAngle,
  Photo,
  PhotoAlbum,
  PhotoCamera,
  PhotoFilter,
  PhotoLibrary,
  PhotoSizeSelectActual,
  PhotoSizeSelectLarge,
  PhotoSizeSelectSmall,
  PictureAsPdf,
  Portrait,
  RemoveRedEye,
  RotateLeft,
  RotateRight,
  Rotate90DegreesCcw,
  ShutterSpeed,
  Slideshow,
  Straighten,
  Style,
  SwitchCamera,
  SwitchVideo,
  TagFaces,
  Texture,
  Timelapse,
  Timer,
  TimerOff,
  Timer10,
  Timer3,
  Tonality,
  Transform,
  Tune,
  ViewComfy,
  ViewCompact,
  Vignette,
  WbAuto,
  WbCloudy,
  WbIncandescent,
  WbIridescent,
  WbSunny
} as Meta;



const TemplateAB: Story<IconProps> = (args) =>
  <>
    <AddAPhoto {...args} />
    <AddPhotoAlternate {...args} />
    <AddToPhotos {...args} />
    <Adjust {...args} />
    <Assistant {...args} />
    <AssistantPhoto {...args} />
    <Audiotrack {...args} />
    <BlurCircular {...args} />
    <BlurLinear {...args} />
    <BlurOff {...args} />
    <BlurOn {...args} />
    <Brightness1 {...args} />
    <Brightness2 {...args} />
    <Brightness3 {...args} />
    <Brightness4 {...args} />
    <Brightness5 {...args} />
    <Brightness6 {...args} />
    <Brightness7 {...args} />
    <BrokenImage {...args} />
    <Brush {...args} />
    <BurstMode {...args} />
  </>

const TemplateCE: Story<IconProps> = (args) =>
  <>
    <Camera {...args} />
    <CameraAlt {...args} />
    <CameraFront {...args} />
    <CameraRear {...args} />
    <CameraRoll {...args} />
    <CenterFocusStrong {...args} />
    <CenterFocusWeak {...args} />
    <Collections {...args} />
    <CollectionsBookmark {...args} />
    <ColorLens {...args} />
    <Colorize {...args} />
    <Compare {...args} />
    <ControlPoint {...args} />
    <ControlPointDuplicate {...args} />
    <Crop {...args} />
    <CropDin {...args} />
    <CropFree {...args} />
    <CropLandscape {...args} />
    <CropOriginal {...args} />
    <CropPortrait {...args} />
    <CropRotate {...args} />
    <CropSquare {...args} />
    <Crop169 {...args} />
    <Crop32 {...args} />
    <Crop54 {...args} />
    <Crop75 {...args} />
    <Dehaze {...args} />
    <Details {...args} />
    <Edit {...args} />
    <Exposure {...args} />
    <ExposureNeg1 {...args} />
    <ExposureNeg2 {...args} />
    <ExposurePlus1 {...args} />
    <ExposurePlus2 {...args} />
    <ExposureZero {...args} />
  </>

const TemplateFG: Story<IconProps> = (args) =>
  <>
    <Filter {...args} />
    <FilterBAndW {...args} />
    <FilterCenterFocus {...args} />
    <FilterDrama {...args} />
    <FilterFrames {...args} />
    <FilterHdr {...args} />
    <FilterNone {...args} />
    <FilterTiltShift {...args} />
    <FilterVintage {...args} />
    <Filter1 {...args} />
    <Filter2 {...args} />
    <Filter3 {...args} />
    <Filter4 {...args} />
    <Filter5 {...args} />
    <Filter6 {...args} />
    <Filter7 {...args} />
    <Filter8 {...args} />
    <Filter9 {...args} />
    <Filter9Plus {...args} />
    <Flare {...args} />
    <FlashAuto {...args} />
    <FlashOff {...args} />
    <FlashOn {...args} />
    <Flip {...args} />
    <Gradient {...args} />
    <Grain {...args} />
    <GridOff {...args} />
    <GridOn {...args} />

  </>
const TemplateHM: Story<IconProps> = (args) =>
  <>
    <HdrOff {...args} />
    <HdrOn {...args} />
    <HdrStrong {...args} />
    <HdrWeak {...args} />
    <Healing {...args} />
    <Image {...args} />
    <ImageAspectRatio {...args} />
    <ImageSearch {...args} />
    <Iso {...args} />
    <Landscape {...args} />
    <LeakAdd {...args} />
    <LeakRemove {...args} />
    <Lens {...args} />
    <LinkedCamera {...args} />
    <Looks {...args} />
    <LooksOne {...args} />
    <LooksTwo {...args} />
    <Looks3 {...args} />
    <Looks4 {...args} />
    <Looks5 {...args} />
    <Looks6 {...args} />
    <Loupe {...args} />
    <MonochromePhotos {...args} />
    <MovieCreation {...args} />
    <MovieFilter {...args} />
    <MusicNote {...args} />
    <MusicOff {...args} />
  </>
const TemplateNR: Story<IconProps> = (args) =>
  <>
    <Nature {...args} />
    <NaturePeople {...args} />
    <NavigateBefore {...args} />
    <NavigateNext {...args} />
    <Palette {...args} />
    <Panorama {...args} />
    <PanoramaFishEye {...args} />
    <PanoramaHorizontal {...args} />
    <PanoramaVertical {...args} />
    <PanoramaWideAngle {...args} />
    <Photo {...args} />
    <PhotoAlbum {...args} />
    <PhotoCamera {...args} />
    <PhotoFilter {...args} />
    <PhotoLibrary {...args} />
    <PhotoSizeSelectActual {...args} />
    <PhotoSizeSelectLarge {...args} />
    <PhotoSizeSelectSmall {...args} />
    <PictureAsPdf {...args} />
    <Portrait {...args} />
    <RemoveRedEye {...args} />
    <RotateLeft {...args} />
    <RotateRight {...args} />
    <Rotate90DegreesCcw {...args} />
  </>
const TemplateSW: Story<IconProps> = (args) =>
  <>
    <ShutterSpeed {...args} />
    <Slideshow {...args} />
    <Straighten {...args} />
    <Style {...args} />
    <SwitchCamera {...args} />
    <SwitchVideo {...args} />
    <TagFaces {...args} />
    <Texture {...args} />
    <Timelapse {...args} />
    <Timer {...args} />
    <TimerOff {...args} />
    <Timer10 {...args} />
    <Timer3 {...args} />
    <Tonality {...args} />
    <Transform {...args} />
    <Tune {...args} />
    <ViewComfy {...args} />
    <ViewCompact {...args} />
    <Vignette {...args} />
    <WbAuto {...args} />
    <WbCloudy {...args} />
    <WbIncandescent {...args} />
    <WbIridescent {...args} />
    <WbSunny {...args} />
  </>

export const ImageIconsAB = TemplateAB.bind({});
ImageIconsAB.args = {
  fillColor: 'red',
};
export const ImageIconsCE = TemplateCE.bind({});
ImageIconsCE.args = {
  fillColor: 'red',
};
export const ImageIconsFG = TemplateFG.bind({});
ImageIconsFG.args = {
  fillColor: 'red',
};
export const ImageIconsHM = TemplateHM.bind({});
ImageIconsHM.args = {
  fillColor: 'red',
};
export const ImageIconsNR = TemplateNR.bind({});
ImageIconsNR.args = {
  fillColor: 'red',
};
export const ImageIconsSW = TemplateSW.bind({});
ImageIconsSW.args = {
  fillColor: 'red',
};




