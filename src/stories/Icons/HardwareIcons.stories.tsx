import React from 'react';
import { Story, Meta } from '@storybook/react/types-6-0';
import { IconProps } from './Icons.interface';
import {
  Cast,
  CastConnected,
  CastForEducation,
  Computer,
  DesktopMac,
  DesktopWindows,
  DeveloperBoard,
  DeviceHub,
  DeviceUnknown,
  DevicesOther,
  Dock,
  Gamepad,
  Headset,
  HeadsetMic,
  Keyboard,
  KeyboardArrowDown,
  KeyboardArrowLeft,
  KeyboardArrowRight,
  KeyboardArrowUp,
  KeyboardBackspace,
  KeyboardCapslock,
  KeyboardHide,
  KeyboardReturn,
  KeyboardTab,
  KeyboardVoice,
  Laptop,
  LaptopChromebook,
  LaptopMac,
  LaptopWindows,
  Memory,
  Mouse,
  PhoneAndroid,
  PhoneIphone,
  Phonelink,
  PhonelinkOff,
  PowerInput,
  RouterIcon,
  Scanner,
  Security,
  SimCard,
  Smartphone,
  Speaker,
  SpeakerGroup,
  Tablet,
  TabletAndroid,
  TabletMac,
  Toys,
  Tv,
  VideogameAsset,
  Watch
} from '@connorfm/amiltone-icons-hardware'



export default {
  title: 'Icons/HardwareIcons',
  component: Cast,
  CastConnected,
  CastForEducation,
  Computer,
  DesktopMac,
  DesktopWindows,
  DeveloperBoard,
  DeviceHub,
  DeviceUnknown,
  DevicesOther,
  Dock,
  Gamepad,
  Headset,
  HeadsetMic,
  Keyboard,
  KeyboardArrowDown,
  KeyboardArrowLeft,
  KeyboardArrowRight,
  KeyboardArrowUp,
  KeyboardBackspace,
  KeyboardCapslock,
  KeyboardHide,
  KeyboardReturn,
  KeyboardTab,
  KeyboardVoice,
  Laptop,
  LaptopChromebook,
  LaptopMac,
  LaptopWindows,
  Memory,
  Mouse,
  PhoneAndroid,
  PhoneIphone,
  Phonelink,
  PhonelinkOff,
  PowerInput,
  RouterIcon,
  Scanner,
  Security,
  SimCard,
  Smartphone,
  Speaker,
  SpeakerGroup,
  Tablet,
  TabletAndroid,
  TabletMac,
  Toys,
  Tv,
  VideogameAsset,
  Watch
} as Meta;



const TemplateCH: Story<IconProps> = (args) =>
  <>
    <Cast {...args} />
    <CastConnected {...args} />
    <CastForEducation {...args} />
    <Computer {...args} />
    <DesktopMac {...args} />
    <DesktopWindows {...args} />
    <DeveloperBoard {...args} />
    <DeviceHub {...args} />
    <DeviceUnknown {...args} />
    <DevicesOther {...args} />
    <Dock {...args} />
    <Gamepad {...args} />
    <Headset {...args} />
    <HeadsetMic {...args} />
  </>


const TemplateKP: Story<IconProps> = (args) =>
  <>
    <Keyboard {...args} />
    <KeyboardArrowDown {...args} />
    <KeyboardArrowLeft {...args} />
    <KeyboardArrowRight {...args} />
    <KeyboardArrowUp {...args} />
    <KeyboardBackspace {...args} />
    <KeyboardCapslock {...args} />
    <KeyboardHide {...args} />
    <KeyboardReturn {...args} />
    <KeyboardTab {...args} />
    <KeyboardVoice {...args} />
    <Laptop {...args} />
    <LaptopChromebook {...args} />
    <LaptopMac {...args} />
    <LaptopWindows {...args} />
    <Memory {...args} />
    <Mouse {...args} />
    <PhoneAndroid {...args} />
    <PhoneIphone {...args} />
    <Phonelink {...args} />
    <PhonelinkOff {...args} />
    <PowerInput {...args} />

  </>

const TemplateRW: Story<IconProps> = (args) =>
  <>
    <RouterIcon {...args} />
    <Scanner {...args} />
    <Security {...args} />
    <SimCard {...args} />
    <Smartphone {...args} />
    <Speaker {...args} />
    <SpeakerGroup {...args} />
    <Tablet {...args} />
    <TabletAndroid {...args} />
    <TabletMac {...args} />
    <Toys {...args} />
    <Tv {...args} />
    <VideogameAsset {...args} />
    <Watch {...args} />
  </>

export const HardwareIconsCH = TemplateCH.bind({});
HardwareIconsCH.args = {
  fillColor: 'red',
};
export const HardwareIconsKP = TemplateKP.bind({});
HardwareIconsKP.args = {
  fillColor: 'red',
};
export const HardwareIconsRW = TemplateRW.bind({});
HardwareIconsRW.args = {
  fillColor: 'red',
};





