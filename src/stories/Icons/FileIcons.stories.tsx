import React from 'react';
import { Story, Meta } from '@storybook/react/types-6-0';
import { IconProps } from './Icons.interface';

import {
  Attachment,
  Cloud,
  CloudCircle,
  CloudDone,
  CloudDownload,
  CloudOff,
  CloudQueue,
  CloudUpload,
  CreateNewFolder,
  Download,
  DownloadDone,
  Folder,
  FolderOpen,
  FolderShared,
  Upload
} from '@connorfm/amiltone-icons-file'



export default {
  title: 'Icons/FileIcons',
  component: Attachment,
  Cloud,
  CloudCircle,
  CloudDone,
  CloudDownload,
  CloudOff,
  CloudQueue,
  CloudUpload,
  CreateNewFolder,
  Download,
  DownloadDone,
  Folder,
  FolderOpen,
  FolderShared,
  Upload
} as Meta;



const Template: Story<IconProps> = (args) =>
  <>
    <Attachment {...args} />
    <Cloud {...args} />
    <CloudCircle {...args} />
    <CloudDone {...args} />
    <CloudDownload {...args} />
    <CloudOff {...args} />
    <CloudQueue {...args} />
    <CloudUpload {...args} />
    <CreateNewFolder {...args} />
    <Download {...args} />
    <DownloadDone {...args} />
    <Folder {...args} />
    <FolderOpen {...args} />
    <FolderShared {...args} />
    <Upload {...args} />
  </>



export const FileIcons = Template.bind({});
FileIcons.args = {
  fillColor: 'red',
};




