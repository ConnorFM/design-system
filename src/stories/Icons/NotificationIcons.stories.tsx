import React from 'react';
import { Story, Meta } from '@storybook/react/types-6-0';
import { IconProps } from './Icons.interface';
import {
  Adb,
  AirlineSeatFlat,
  AirlineSeatFlatAngled,
  AirlineSeatIndividualSuite,
  AirlineSeatLegroomExtra,
  AirlineSeatLegroomNormal,
  AirlineSeatLegroomReduced,
  AirlineSeatReclineExtra,
  AirlineSeatReclineNormal,
  BluetoothAudio,
  ConfirmationNumber,
  DiscFull,
  DoDisturb,
  DoDisturbAlt,
  DoDisturbOff,
  DoDisturbOn,
  DriveEta,
  EnhancedEncryption,
  EventAvailable,
  EventBusy,
  EventNote,
  FolderSpecial,
  LiveTv,
  Mms,
  More,
  NetworkCheck,
  NetworkLocked,
  NoEncryption,
  NoEncryptionGmailerrorred,
  OndemandVideo,
  PersonalVideo,
  PhoneBluetoothSpeaker,
  PhoneCallback,
  PhoneForwarded,
  PhoneInTalk,
  PhoneLocked,
  PhoneMissed,
  PhonePaused,
  Power,
  PowerOff,
  PriorityHigh,
  RvHookup,
  SdCard,
  SdCardAlert,
  Sms,
  SmsFailed,
  Sync,
  SyncDisabled,
  SyncProblem,
  SystemUpdate,
  TapAndPlay,
  TimeToLeave,
  TvOff,
  Vibration,
  VoiceChat,
  VpnLock,
  Wc,
  Wifi,
  WifiOff
} from '@connorfm/amiltone-icons-notification'



export default {
  title: 'Icons/NotificationIcons',
  component: Adb,
  AirlineSeatFlat,
  AirlineSeatFlatAngled,
  AirlineSeatIndividualSuite,
  AirlineSeatLegroomExtra,
  AirlineSeatLegroomNormal,
  AirlineSeatLegroomReduced,
  AirlineSeatReclineExtra,
  AirlineSeatReclineNormal,
  BluetoothAudio,
  ConfirmationNumber,
  DiscFull,
  DoDisturb,
  DoDisturbAlt,
  DoDisturbOff,
  DoDisturbOn,
  DriveEta,
  EnhancedEncryption,
  EventAvailable,
  EventBusy,
  EventNote,
  FolderSpecial,
  LiveTv,
  Mms,
  More,
  NetworkCheck,
  NetworkLocked,
  NoEncryption,
  NoEncryptionGmailerrorred,
  OndemandVideo,
  PersonalVideo,
  PhoneBluetoothSpeaker,
  PhoneCallback,
  PhoneForwarded,
  PhoneInTalk,
  PhoneLocked,
  PhoneMissed,
  PhonePaused,
  Power,
  PowerOff,
  PriorityHigh,
  RvHookup,
  SdCard,
  SdCardAlert,
  Sms,
  SmsFailed,
  Sync,
  SyncDisabled,
  SyncProblem,
  SystemUpdate,
  TapAndPlay,
  TimeToLeave,
  TvOff,
  Vibration,
  VoiceChat,
  VpnLock,
  Wc,
  Wifi,
  WifiOff
} as Meta;



const TemplateAD: Story<IconProps> = (args) =>
  <>
    <Adb {...args} />
    <AirlineSeatFlat {...args} />
    <AirlineSeatFlatAngled {...args} />
    <AirlineSeatIndividualSuite {...args} />
    <AirlineSeatLegroomExtra {...args} />
    <AirlineSeatLegroomNormal {...args} />
    <AirlineSeatLegroomReduced {...args} />
    <AirlineSeatReclineExtra {...args} />
    <AirlineSeatReclineNormal {...args} />
    <BluetoothAudio {...args} />
    <ConfirmationNumber {...args} />
    <DiscFull {...args} />
    <DoDisturb {...args} />
    <DoDisturbAlt {...args} />
    <DoDisturbOff {...args} />
    <DoDisturbOn {...args} />
    <DriveEta {...args} />
  </>

const TemplateEP: Story<IconProps> = (args) =>
  <>
    <EnhancedEncryption {...args} />
    <EventAvailable {...args} />
    <EventBusy {...args} />
    <EventNote {...args} />
    <FolderSpecial {...args} />
    <LiveTv {...args} />
    <Mms {...args} />
    <More {...args} />
    <NetworkCheck {...args} />
    <NetworkLocked {...args} />
    <NoEncryption {...args} />
    <NoEncryptionGmailerrorred {...args} />
    <OndemandVideo {...args} />
    <PersonalVideo {...args} />
    <PhoneBluetoothSpeaker {...args} />
    <PhoneCallback {...args} />
    <PhoneForwarded {...args} />
    <PhoneInTalk {...args} />
    <PhoneLocked {...args} />
    <PhoneMissed {...args} />
    <PhonePaused {...args} />
    <Power {...args} />
    <PowerOff {...args} />
    <PriorityHigh {...args} />
  </>

const TemplateRW: Story<IconProps> = (args) =>
  <>
    <RvHookup {...args} />
    <SdCard {...args} />
    <SdCardAlert {...args} />
    <Sms {...args} />
    <SmsFailed {...args} />
    <Sync {...args} />
    <SyncDisabled {...args} />
    <SyncProblem {...args} />
    <SystemUpdate {...args} />
    <TapAndPlay {...args} />
    <TimeToLeave {...args} />
    <TvOff {...args} />
    <Vibration {...args} />
    <VoiceChat {...args} />
    <VpnLock {...args} />
    <Wc {...args} />
    <Wifi {...args} />
    <WifiOff {...args} />
  </>

export const NotificationIconsAD = TemplateAD.bind({});
NotificationIconsAD.args = {
  fillColor: 'red',
};
export const NotificationIconsEP = TemplateEP.bind({});
NotificationIconsEP.args = {
  fillColor: 'red',
};
export const NotificationIconsRW = TemplateRW.bind({});
NotificationIconsRW.args = {
  fillColor: 'red',
};




