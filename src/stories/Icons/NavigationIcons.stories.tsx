import React from 'react';
import { Story, Meta } from '@storybook/react/types-6-0';
import { IconProps } from './Icons.interface';
import {
  Apps,
  ArrowBack,
  ArrowBackIos,
  ArrowDownward,
  ArrowDropDown,
  ArrowDropDownCircle,
  ArrowDropUp,
  ArrowForward,
  ArrowForwardIos,
  ArrowLeft,
  ArrowRight,
  ArrowUpward,
  Cancel,
  Check,
  ChevronLeft,
  ChevronRight,
  Close,
  ExpandLess,
  ExpandMore,
  FirstPage,
  Fullscreen,
  FullscreenExit,
  LastPage,
  Menu,
  MoreHoriz,
  MoreVert,
  Refresh,
  SubdirectoryArrowLeft,
  SubdirectoryArrowRight,
  UnfoldLess,
  UnfoldMore
} from '@connorfm/amiltone-icons-navigation'



export default {
  title: 'Icons/NavigationIcons',
  component: Apps,
  ArrowBack,
  ArrowBackIos,
  ArrowDownward,
  ArrowDropDown,
  ArrowDropDownCircle,
  ArrowDropUp,
  ArrowForward,
  ArrowForwardIos,
  ArrowLeft,
  ArrowRight,
  ArrowUpward,
  Cancel,
  Check,
  ChevronLeft,
  ChevronRight,
  Close,
  ExpandLess,
  ExpandMore,
  FirstPage,
  Fullscreen,
  FullscreenExit,
  LastPage,
  Menu,
  MoreHoriz,
  MoreVert,
  Refresh,
  SubdirectoryArrowLeft,
  SubdirectoryArrowRight,
  UnfoldLess,
  UnfoldMore,
} as Meta;



const TemplateAC: Story<IconProps> = (args) =>
  <>
    <Apps {...args} />
    <ArrowBack {...args} />
    <ArrowBackIos {...args} />
    <ArrowDownward {...args} />
    <ArrowDropDown {...args} />
    <ArrowDropDownCircle {...args} />
    <ArrowDropUp {...args} />
    <ArrowForward {...args} />
    <ArrowForwardIos {...args} />
    <ArrowLeft {...args} />
    <ArrowRight {...args} />
    <ArrowUpward {...args} />
    <Cancel {...args} />
    <Check {...args} />
    <ChevronLeft {...args} />
    <ChevronRight {...args} />
    <Close {...args} />
  </>

const TemplateEU: Story<IconProps> = (args) =>
  <>
    <ExpandLess {...args} />
    <ExpandMore {...args} />
    <FirstPage {...args} />
    <Fullscreen {...args} />
    <FullscreenExit {...args} />
    <LastPage {...args} />
    <Menu {...args} />
    <MoreHoriz {...args} />
    <MoreVert {...args} />
    <Refresh {...args} />
    <SubdirectoryArrowLeft {...args} />
    <SubdirectoryArrowRight {...args} />
    <UnfoldLess {...args} />
    <UnfoldMore {...args} />
  </>

export const NavigationIconsAC = TemplateAC.bind({});
NavigationIconsAC.args = {
  fillColor: 'red',
};
export const NavigationIconsEU = TemplateEU.bind({});
NavigationIconsEU.args = {
  fillColor: 'red',
};
