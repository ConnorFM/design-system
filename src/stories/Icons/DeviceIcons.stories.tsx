import React from 'react';
import { Story, Meta } from '@storybook/react/types-6-0';
import { IconProps } from './Icons.interface';
import {
  AccessAlarm,
  AccessAlarms,
  AccessTime,
  AddAlarm,
  AddToHomeScreen,
  AirplanemodeActive,
  AirplanemodeInactive,
  BatteryAlert,
  BatteryChargingFull,
  BatteryFull,
  BatteryStd,
  BatteryUnknown,
  Bluetooth,
  BluetoothConnected,
  BluetoothDisabled,
  BluetoothSearching,
  BrightnessAuto,
  BrightnessHigh,
  BrightnessLow,
  BrightnessMedium,
  DataUsage,
  DeveloperMode,
  Devices,
  Dvr,
  Eq,
  GpsFixed,
  GpsNotFixed,
  GpsOff,
  LocationDisabled,
  LocationSearching,
  MobileFriendly,
  MobileOff,
  Nfc,
  ScreenLockLandscape,
  ScreenLockPortrait,
  ScreenLockRotation,
  ScreenRotation,
  SdStorage,
  SettingsSystemDaydream,
  SignalCellularAlt,
  SignalCellularConnectedNoInternet,
  SignalCellularNoSim,
  SignalCellularNull,
  SignalCellularOff,
  SignalWifiOff,
  SignalWifiStatusbarNull,
  Storage,
  Thermostat,
  Usb,
  Wallpaper,
  Widgets,
  WifiLock,
  WifiTethering
} from '@connorfm/amiltone-icons-device';

export default {
  title: 'Icons/DeviceIcons',
  component: AccessAlarm,
  AccessAlarms,
  AccessTime,
  AddAlarm,
  AddToHomeScreen,
  AirplanemodeActive,
  AirplanemodeInactive,
  BatteryAlert,
  BatteryChargingFull,
  BatteryFull,
  BatteryStd,
  BatteryUnknown,
  Bluetooth,
  BluetoothConnected,
  BluetoothDisabled,
  BluetoothSearching,
  BrightnessAuto,
  BrightnessHigh,
  BrightnessLow,
  BrightnessMedium,
  DataUsage,
  DeveloperMode,
  Devices,
  Dvr,
  Eq,
  GpsFixed,
  GpsNotFixed,
  GpsOff,
  LocationDisabled,
  LocationSearching,
  MobileFriendly,
  MobileOff,
  Nfc,
  ScreenLockLandscape,
  ScreenLockPortrait,
  ScreenLockRotation,
  ScreenRotation,
  SdStorage,
  SettingsSystemDaydream,
  SignalCellularAlt,
  SignalCellularConnectedNoInternet,
  SignalCellularNoSim,
  SignalCellularNull,
  SignalCellularOff,
  SignalWifiOff,
  SignalWifiStatusbarNull,
  Storage,
  Thermostat,
  Usb,
  Wallpaper,
  Widgets,
  WifiLock,
  WifiTethering
} as Meta;



const TemplateAB: Story<IconProps> = (args) =>
  <>
    <AccessAlarm {...args} />
    <AccessAlarms {...args} />
    <AccessTime {...args} />
    <AddAlarm {...args} />
    <AddToHomeScreen {...args} />
    <AirplanemodeActive {...args} />
    <AirplanemodeInactive {...args} />
    <BatteryAlert {...args} />
    <BatteryChargingFull {...args} />
    <BatteryFull {...args} />
    <BatteryStd {...args} />
    <BatteryUnknown {...args} />
    <Bluetooth {...args} />
    <BluetoothConnected {...args} />
    <BluetoothDisabled {...args} />
    <BluetoothSearching {...args} />
    <BrightnessAuto {...args} />
    <BrightnessHigh {...args} />
    <BrightnessLow {...args} />
    <BrightnessMedium {...args} />
  </>
const TemplateDS: Story<IconProps> = (args) =>
  <>
    <DataUsage {...args} />
    <DeveloperMode {...args} />
    <Devices {...args} />
    <Dvr {...args} />
    <Eq {...args} />
    <GpsFixed {...args} />
    <GpsNotFixed {...args} />
    <GpsOff {...args} />
    <LocationDisabled {...args} />
    <LocationSearching {...args} />
    <MobileFriendly {...args} />
    <MobileOff {...args} />
    <Nfc {...args} />
    <ScreenLockLandscape {...args} />
    <ScreenLockPortrait {...args} />
    <ScreenLockRotation {...args} />
    <ScreenRotation {...args} />
    <SdStorage {...args} />
    <SettingsSystemDaydream {...args} />
    <SignalCellularAlt {...args} />
    <SignalCellularConnectedNoInternet {...args} />
    <SignalCellularNoSim {...args} />
    <SignalCellularNull {...args} />
    <SignalCellularOff {...args} />
    <SignalWifiOff {...args} />
    <SignalWifiStatusbarNull {...args} />
    <Storage {...args} />
  </>
const TemplateTW: Story<IconProps> = (args) =>
  <>
    <Thermostat {...args} />
    <Usb {...args} />
    <Wallpaper {...args} />
    <Widgets {...args} />
    <WifiLock {...args} />
    <WifiTethering {...args} />
  </>


export const DeviceIconsAB = TemplateAB.bind({});
DeviceIconsAB.args = {
  fillColor: 'red',
};
export const DeviceIconsDS = TemplateDS.bind({});
DeviceIconsDS.args = {
  fillColor: 'red',
};
export const DeviceIconsTW = TemplateTW.bind({});
DeviceIconsTW.args = {
  fillColor: 'red',
};




