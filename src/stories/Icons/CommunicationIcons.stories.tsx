import React from 'react';
import { Story, Meta } from '@storybook/react/types-6-0';
import { IconProps } from './Icons.interface';
import {
  AddCall,
  Business,
  Call,
  CallEnd,
  CallMade,
  CallMerge,
  CallMissed,
  CallMissedOutgoing,
  CallReceived,
  CallSplit,
  CancelPresentation,
  Chat,
  ChatBubble,
  ChatBubbleOutline,
  ClearAll,
  Comment,
  ContactMail,
  ContactPhone,
  Contacts,
  DesktopAccessDisabled,
  DialerSip,
  Dialpad,
  DomainDisabled,
  Duo,
  Email,
  Forum,
  ImportContacts,
  ImportExport,
  InvertColorsOff,
  ListAlt,
  LiveHelp,
  LocationOff,
  LocationOn,
  MailOutline,
  Message,
  MobileScreenShare,
  NoSim,
  PausePresentation,
  PersonAddDisabled,
  Phone,
  PhonelinkErase,
  PhonelinkLock,
  PhonelinkRing,
  PhonelinkSetup,
  PortableWifiOff,
  PresentToAll,
  PrintDisabled,
  RingVolume,
  RssFeed,
  ScreenShare,
  SentimentSatisfiedAlt,
  SpeakerPhone,
  StayCurrentLandscape,
  StayCurrentPortrait,
  StayPrimaryLandscape,
  StayPrimaryPortrait,
  StopScreenShare,
  SwapCalls,
  Textsms,
  Unsubscribe,
  Voicemail,
  VpnKey,
} from '@connorfm/amiltone-icons-communication'


export default {
  title: 'Icons/CommunicationIcons',
  component: AddCall,
  Business,
  Call,
  CallEnd,
  CallMade,
  CallMerge,
  CallMissed,
  CallMissedOutgoing,
  CallReceived,
  CallSplit,
  CancelPresentation,
  Chat,
  ChatBubble,
  ChatBubbleOutline,
  ClearAll,
  Comment,
  ContactMail,
  ContactPhone,
  Contacts,
  DesktopAccessDisabled,
  DialerSip,
  Dialpad,
  DomainDisabled,
  Duo,
  Email,
  Forum,
  ImportContacts,
  ImportExport,
  InvertColorsOff,
  ListAlt,
  LiveHelp,
  LocationOff,
  LocationOn,
  MailOutline,
  Message,
  MobileScreenShare,
  NoSim,
  PausePresentation,
  PersonAddDisabled,
  Phone,
  PhonelinkErase,
  PhonelinkLock,
  PhonelinkRing,
  PhonelinkSetup,
  PortableWifiOff,
  PresentToAll,
  PrintDisabled,
  RingVolume,
  RssFeed,
  ScreenShare,
  SentimentSatisfiedAlt,
  SpeakerPhone,
  StayCurrentLandscape,
  StayCurrentPortrait,
  StayPrimaryLandscape,
  StayPrimaryPortrait,
  StopScreenShare,
  SwapCalls,
  Textsms,
  Unsubscribe,
  Voicemail,
  VpnKey,
} as Meta;



const TemplateA: Story<IconProps> = (args) =>
  <AddCall {...args} />
const TemplateB: Story<IconProps> = (args) =>
  <Business {...args} />;
const TemplateC: Story<IconProps> = (args) =>
  <>
    <Call {...args} />
    <CallEnd {...args} />
    <CallMade {...args} />
    <CallMerge {...args} />
    <CallMissed {...args} />
    <CallMissedOutgoing {...args} />
    <CallReceived {...args} />
    <CallSplit {...args} />
    <CancelPresentation {...args} />
    <Chat {...args} />
    <ChatBubble {...args} />
    <ChatBubbleOutline {...args} />
    <ClearAll {...args} />
    <Comment {...args} />
    <ContactMail {...args} />
    <ContactPhone {...args} />
    <Contacts {...args} />
  </>;
const TemplateD: Story<IconProps> = (args) =>
  <>
    <DesktopAccessDisabled {...args} />
    <DialerSip {...args} />
    <Dialpad {...args} />
    <DomainDisabled {...args} />
    <Duo {...args} />
  </>
const TemplateE: Story<IconProps> = (args) =>
  <Email {...args} />
const TemplateF: Story<IconProps> = (args) =>
  <Forum {...args} />
const TemplateI: Story<IconProps> = (args) =>
  <>
    <ImportContacts {...args} />
    <ImportExport {...args} />
    <InvertColorsOff {...args} />
  </>
const TemplateL: Story<IconProps> = (args) =>
  <>
    <ListAlt {...args} />
    <LiveHelp {...args} />
    <LocationOff {...args} />
    <LocationOn {...args} />
  </>
const TemplateM: Story<IconProps> = (args) =>
  <>
    <MailOutline {...args} />
    <Message {...args} />
    <MobileScreenShare {...args} />
  </>
const TemplateN: Story<IconProps> = (args) =>
  <NoSim {...args} />
const TemplateP: Story<IconProps> = (args) =>
  <>
    <PausePresentation {...args} />
    <PersonAddDisabled {...args} />
    <Phone {...args} />
    <PhonelinkErase {...args} />
    <PhonelinkLock {...args} />
    <PhonelinkRing {...args} />
    <PhonelinkSetup {...args} />
    <PortableWifiOff {...args} />
    <PresentToAll {...args} />
    <PrintDisabled {...args} />
  </>
const TemplateR: Story<IconProps> = (args) =>
  <>
    <RingVolume {...args} />
    <RssFeed {...args} />
  </>
const TemplateS: Story<IconProps> = (args) =>
  <>
    <ScreenShare {...args} />
    <SentimentSatisfiedAlt {...args} />
    <SpeakerPhone {...args} />
    <StayCurrentLandscape {...args} />
    <StayCurrentPortrait {...args} />
    <StayPrimaryLandscape {...args} />
    <StayPrimaryPortrait {...args} />
    <StopScreenShare {...args} />
    <SwapCalls {...args} />
  </>
const TemplateT: Story<IconProps> = (args) =>
  <Textsms {...args} />
const TemplateU: Story<IconProps> = (args) =>
  <Unsubscribe {...args} />
const TemplateV: Story<IconProps> = (args) =>
  <>
    <Voicemail {...args} />
    <VpnKey {...args} />
  </>


export const CommunicationIconsA = TemplateA.bind({});
CommunicationIconsA.args = {
  fillColor: 'red',
};
export const CommunicationIconsB = TemplateB.bind({});
CommunicationIconsB.args = {
  fillColor: 'red',
};
export const CommunicationIconsC = TemplateC.bind({});
CommunicationIconsC.args = {
  fillColor: 'red',
};
export const CommunicationIconsD = TemplateD.bind({});
CommunicationIconsD.args = {
  fillColor: 'red',
};
export const CommunicationIconsE = TemplateE.bind({});
CommunicationIconsE.args = {
  fillColor: 'red',
};
export const CommunicationIconsF = TemplateF.bind({});
CommunicationIconsF.args = {
  fillColor: 'red',
};
export const CommunicationIconsI = TemplateI.bind({});
CommunicationIconsI.args = {
  fillColor: 'red',
};
export const CommunicationIconsL = TemplateL.bind({});
CommunicationIconsL.args = {
  fillColor: 'red',
};
export const CommunicationIconsM = TemplateM.bind({});
CommunicationIconsM.args = {
  fillColor: 'red',
};
export const CommunicationIconsN = TemplateN.bind({});
CommunicationIconsN.args = {
  fillColor: 'red',
};
export const CommunicationIconsP = TemplateP.bind({});
CommunicationIconsP.args = {
  fillColor: 'red',
};
export const CommunicationIconsR = TemplateR.bind({});
CommunicationIconsR.args = {
  fillColor: 'red',
};
export const CommunicationIconsS = TemplateS.bind({});
CommunicationIconsS.args = {
  fillColor: 'red',
};
export const CommunicationIconsT = TemplateT.bind({});
CommunicationIconsT.args = {
  fillColor: 'red',
};
export const CommunicationIconsU = TemplateU.bind({});
CommunicationIconsU.args = {
  fillColor: 'red',
};
export const CommunicationIconsV = TemplateV.bind({});
CommunicationIconsV.args = {
  fillColor: 'red',
};



