import React from 'react';
import { Story, Meta } from '@storybook/react/types-6-0';
import { IconProps } from './Icons.interface';
import {
  Add,
  AddBox,
  AddCircle,
  AddCircleOutline,
  Archive,
  Attribution,
  Backspace,
  Ballot,
  Block,
  Clear,
  Copy,
  Create,
  Cut,
  DeleteSweep,
  Drafts,
  FileCopy,
  FilterList,
  Flag,
  FontDownload,
  Forward,
  Gesture,
  HowToReg,
  HowToVote,
  Inbox,
  LinkOff,
  LowPriority,
  Mail,
  Markunread,
  MoveToInbox,
  NextWeek,
  OutlinedFlag,
  Paste,
  Redo,
  Remove,
  RemoveCircle,
  RemoveCircleOutline,
  Reply,
  ReplyAll,
  Report,
  ReportGmailerrorred,
  ReportOff,
  Save,
  SaveAlt,
  SelectAll,
  Send,
  Sort,
  TextFormat,
  Unarchive,
  Undo,
  Waves,
  Weekend,
  WhereToVote
} from '@connorfm/amiltone-icons-content'



export default {
  title: 'Icons/ContentIcons',
  component: Add,
  AddBox,
  AddCircle,
  AddCircleOutline,
  Archive,
  Attribution,
  Backspace,
  Ballot,
  Block,
  Clear,
  Copy,
  Create,
  Cut,
  DeleteSweep,
  Drafts,
  FileCopy,
  FilterList,
  Flag,
  FontDownload,
  Forward,
  Gesture,
  HowToReg,
  HowToVote,
  Inbox,
  LinkOff,
  LowPriority,
  Mail,
  Markunread,
  MoveToInbox,
  NextWeek,
  OutlinedFlag,
  Paste,
  Redo,
  Remove,
  RemoveCircle,
  RemoveCircleOutline,
  Reply,
  ReplyAll,
  Report,
  ReportGmailerrorred,
  ReportOff,
  Save,
  SaveAlt,
  SelectAll,
  Send,
  Sort,
  TextFormat,
  Unarchive,
  Undo,
  Waves,
  Weekend,
  WhereToVote
} as Meta;



const TemplateA: Story<IconProps> = (args) =>
  <>
    <Add {...args} />
    <AddBox {...args} />
    <AddCircle {...args} />
    <AddCircleOutline {...args} />
    <Archive {...args} />
    <Attribution {...args} />
  </>
const TemplateB: Story<IconProps> = (args) =>
  <>
    <Backspace {...args} />
    <Ballot {...args} />
    <Block {...args} />
  </>
const TemplateC: Story<IconProps> = (args) =>
  <>
    <Clear {...args} />
    <Copy {...args} />
    <Create {...args} />
    <Cut {...args} />
  </>
const TemplateD: Story<IconProps> = (args) =>
  <>
    <DeleteSweep {...args} />
    <Drafts {...args} />
  </>
const TemplateF: Story<IconProps> = (args) =>
  <>
    <FileCopy {...args} />
    <FilterList {...args} />
    <Flag {...args} />
    <FontDownload {...args} />
    <Forward {...args} />
  </>
const TemplateG: Story<IconProps> = (args) =>
  <Gesture {...args} />
const TemplateH: Story<IconProps> = (args) =>
  <>
    <HowToReg {...args} />
    <HowToVote {...args} />
  </>
const TemplateI: Story<IconProps> = (args) =>
  <Inbox {...args} />
const TemplateL: Story<IconProps> = (args) =>
  <>
    <LinkOff {...args} />
    <LowPriority {...args} />
  </>
const TemplateM: Story<IconProps> = (args) =>
  <>
    <Mail {...args} />
    <Markunread {...args} />
    <MoveToInbox {...args} />
  </>
const TemplateN: Story<IconProps> = (args) =>
  <NextWeek {...args} />
const TemplateO: Story<IconProps> = (args) =>
  <OutlinedFlag {...args} />
const TemplateP: Story<IconProps> = (args) =>
  <Paste {...args} />
const TemplateR: Story<IconProps> = (args) =>
  <>
    <Redo {...args} />
    <Remove {...args} />
    <RemoveCircle {...args} />
    <RemoveCircleOutline {...args} />
    <Reply {...args} />
    <ReplyAll {...args} />
    <Report {...args} />
    <ReportGmailerrorred {...args} />
    <ReportOff {...args} />
  </>
const TemplateS: Story<IconProps> = (args) =>
  <>
    <Save {...args} />
    <SaveAlt {...args} />
    <SelectAll {...args} />
    <Send {...args} />
    <Sort {...args} />
  </>
const TemplateT: Story<IconProps> = (args) =>
  <TextFormat {...args} />
const TemplateU: Story<IconProps> = (args) =>
  <>
    <Unarchive {...args} />
    <Undo {...args} />
  </>
const TemplateW: Story<IconProps> = (args) =>
  <>
    <Waves {...args} />
    <Weekend {...args} />
    <WhereToVote {...args} />
  </>;

export const ContentIconsA = TemplateA.bind({});
ContentIconsA.args = {
  fillColor: 'red',
};
export const ContentIconsB = TemplateB.bind({});
ContentIconsB.args = {
  fillColor: 'red',
};
export const ContentIconsC = TemplateC.bind({});
ContentIconsC.args = {
  fillColor: 'red',
};
export const ContentIconsD = TemplateD.bind({});
ContentIconsD.args = {
  fillColor: 'red',
};
export const ContentIconsF = TemplateF.bind({});
ContentIconsF.args = {
  fillColor: 'red',
};
export const ContentIconsG = TemplateG.bind({});
ContentIconsG.args = {
  fillColor: 'red',
};
export const ContentIconsH = TemplateH.bind({});
ContentIconsH.args = {
  fillColor: 'red',
};
export const ContentIconsI = TemplateI.bind({});
ContentIconsI.args = {
  fillColor: 'red',
};
export const ContentIconsL = TemplateL.bind({});
ContentIconsL.args = {
  fillColor: 'red',
};
export const ContentIconsM = TemplateM.bind({});
ContentIconsM.args = {
  fillColor: 'red',
};
export const ContentIconsN = TemplateN.bind({});
ContentIconsN.args = {
  fillColor: 'red',
};
export const ContentIconsO = TemplateO.bind({});
ContentIconsO.args = {
  fillColor: 'red',
};
export const ContentIconsP = TemplateP.bind({});
ContentIconsP.args = {
  fillColor: 'red',
};
export const ContentIconsR = TemplateR.bind({});
ContentIconsR.args = {
  fillColor: 'red',
};
export const ContentIconsS = TemplateS.bind({});
ContentIconsS.args = {
  fillColor: 'red',
};
export const ContentIconsT = TemplateT.bind({});
ContentIconsT.args = {
  fillColor: 'red',
};
export const ContentIconsU = TemplateU.bind({});
ContentIconsU.args = {
  fillColor: 'red',
};
export const ContentIconsW = TemplateW.bind({});
ContentIconsW.args = {
  fillColor: 'red',
};






