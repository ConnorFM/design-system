import React from 'react';
import { Story, Meta } from '@storybook/react/types-6-0';
import { IconProps } from './Icons.interface';
import {
  ThreeSixty,
  AddLocation,
  Atm,
  Beenhere,
  Category,
  CompassCalibration,
  DepartureBoard,
  Directions,
  DirectionsBike,
  DirectionsBoat,
  DirectionsBus,
  DirectionsCar,
  DirectionsRailway,
  DirectionsRun,
  DirectionsSubway,
  DirectionsTransit,
  DirectionsWalk,
  EditAttributes,
  EditLocation,
  EvStation,
  Fastfood,
  Flight,
  Hotel,
  Layers,
  LayersClear,
  LocalActivity,
  LocalAirport,
  LocalAtm,
  LocalBar,
  LocalCafe,
  LocalCarWash,
  LocalConvenienceStore,
  LocalDining,
  LocalDrink,
  LocalFlorist,
  LocalGasStation,
  LocalGroceryStore,
  LocalHospital,
  LocalHotel,
  LocalLaundryService,
  LocalLibrary,
  LocalMall,
  LocalMovies,
  LocalOffer,
  LocalParking,
  LocalPharmacy,
  LocalPhone,
  LocalPizza,
  LocalPlay,
  LocalPostOffice,
  LocalPrintshop,
  LocalSee,
  LocalShipping,
  LocalTaxi,
  Map,
  Money,
  MyLocation,
  Navigation,
  NearMe,
  NotListedLocation,
  PersonPin,
  PersonPinCircle,
  PinDrop,
  Place,
  RateReview,
  Restaurant,
  RestaurantMenu,
  Satellite,
  StoreMallDirectory,
  Streetview,
  Subway,
  Terrain,
  Trafficon,
  Train,
  Tram,
  TransferWithinAStation,
  TransitEnterexit,
  TripOrigin,
  ZoomOutMap
} from '@connorfm/amiltone-icons-maps'



export default {
  title: 'Icons/MapsIcons',
  component: ThreeSixty,
  AddLocation,
  Atm,
  Beenhere,
  Category,
  CompassCalibration,
  DepartureBoard,
  Directions,
  DirectionsBike,
  DirectionsBoat,
  DirectionsBus,
  DirectionsCar,
  DirectionsRailway,
  DirectionsRun,
  DirectionsSubway,
  DirectionsTransit,
  DirectionsWalk,
  EditAttributes,
  EditLocation,
  EvStation,
  Fastfood,
  Flight,
  Hotel,
  Layers,
  LayersClear,
  LocalActivity,
  LocalAirport,
  LocalAtm,
  LocalBar,
  LocalCafe,
  LocalCarWash,
  LocalConvenienceStore,
  LocalDining,
  LocalDrink,
  LocalFlorist,
  LocalGasStation,
  LocalGroceryStore,
  LocalHospital,
  LocalHotel,
  LocalLaundryService,
  LocalLibrary,
  LocalMall,
  LocalMovies,
  LocalOffer,
  LocalParking,
  LocalPharmacy,
  LocalPhone,
  LocalPizza,
  LocalPlay,
  LocalPostOffice,
  LocalPrintshop,
  LocalSee,
  LocalShipping,
  LocalTaxi,
  Map,
  Money,
  MyLocation,
  Navigation,
  NearMe,
  NotListedLocation,
  PersonPin,
  PersonPinCircle,
  PinDrop,
  Place,
  RateReview,
  Restaurant,
  RestaurantMenu,
  Satellite,
  StoreMallDirectory,
  Streetview,
  Subway,
  Terrain,
  Trafficon,
  Train,
  Tram,
  TransferWithinAStation,
  TransitEnterexit,
  TripOrigin,
  ZoomOutMap
} as Meta;



const TemplateAE: Story<IconProps> = (args) =>
  <>
    <AddLocation {...args} />
    <Atm {...args} />
    <Beenhere {...args} />
    <Category {...args} />
    <CompassCalibration {...args} />
    <DepartureBoard {...args} />
    <Directions {...args} />
    <DirectionsBike {...args} />
    <DirectionsBoat {...args} />
    <DirectionsBus {...args} />
    <DirectionsCar {...args} />
    <DirectionsRailway {...args} />
    <DirectionsRun {...args} />
    <DirectionsSubway {...args} />
    <DirectionsTransit {...args} />
    <DirectionsWalk {...args} />
    <EditAttributes {...args} />
    <EditLocation {...args} />
    <EvStation {...args} />
  </>

const TemplateFL: Story<IconProps> = (args) =>
  <>
    <Fastfood {...args} />
    <Flight {...args} />
    <Hotel {...args} />
    <Layers {...args} />
    <LayersClear {...args} />
    <LocalActivity {...args} />
    <LocalAirport {...args} />
    <LocalAtm {...args} />
    <LocalBar {...args} />
    <LocalCafe {...args} />
    <LocalCarWash {...args} />
    <LocalConvenienceStore {...args} />
    <LocalDining {...args} />
    <LocalDrink {...args} />
    <LocalFlorist {...args} />
    <LocalGasStation {...args} />
    <LocalGroceryStore {...args} />
    <LocalHospital {...args} />
    <LocalHotel {...args} />
    <LocalLaundryService {...args} />
    <LocalLibrary {...args} />
    <LocalMall {...args} />
    <LocalMovies {...args} />
    <LocalOffer {...args} />
    <LocalParking {...args} />
    <LocalPharmacy {...args} />
    <LocalPhone {...args} />
    <LocalPizza {...args} />
    <LocalPlay {...args} />
    <LocalPostOffice {...args} />
    <LocalPrintshop {...args} />
    <LocalSee {...args} />
    <LocalShipping {...args} />
    <LocalTaxi {...args} />
  </>

const TemplateMZ: Story<IconProps> = (args) =>
  <>
    <Map {...args} />
    <Money {...args} />
    <MyLocation {...args} />
    <Navigation {...args} />
    <NearMe {...args} />
    <NotListedLocation {...args} />
    <PersonPin {...args} />
    <PersonPinCircle {...args} />
    <PinDrop {...args} />
    <Place {...args} />
    <RateReview {...args} />
    <Restaurant {...args} />
    <RestaurantMenu {...args} />
    <Satellite {...args} />
    <StoreMallDirectory {...args} />
    <Streetview {...args} />
    <Subway {...args} />
    <Terrain {...args} />
    <ThreeSixty {...args} />
    <Trafficon {...args} />
    <Train {...args} />
    <Tram {...args} />
    <TransferWithinAStation {...args} />
    <TransitEnterexit {...args} />
    <TripOrigin {...args} />
    <ZoomOutMap {...args} />
  </>


export const MapsIconsAE = TemplateAE.bind({});
MapsIconsAE.args = {
  fillColor: 'red',
};
export const MapsIconsFL = TemplateFL.bind({});
MapsIconsFL.args = {
  fillColor: 'red',
};
export const MapsIconsMZ = TemplateMZ.bind({});
MapsIconsMZ.args = {
  fillColor: 'red',
};




