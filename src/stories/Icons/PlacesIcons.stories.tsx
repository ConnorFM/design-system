import React from 'react';
import { Story, Meta } from '@storybook/react/types-6-0';
import { IconProps } from './Icons.interface';
import {
  AcUnit,
  AirportShuttle,
  AllInclusive,
  BeachAccess,
  BusinessCenter,
  Casino,
  ChildCare,
  ChildFriendly,
  FitnessCenter,
  FreeBreakfast,
  GolfCourse,
  HotTub,
  Kitchen,
  MeetingRoom,
  NoMeetingRoom,
  Pool,
  RoomService,
  RvHookup,
  SmokeFree,
  SmokingRooms,
  Spa
} from '@connorfm/amiltone-icons-places'



export default {
  title: 'Icons/PlacesIcons',
  component: AcUnit,
  AirportShuttle,
  AllInclusive,
  BeachAccess,
  BusinessCenter,
  Casino,
  ChildCare,
  ChildFriendly,
  FitnessCenter,
  FreeBreakfast,
  GolfCourse,
  HotTub,
  Kitchen,
  MeetingRoom,
  NoMeetingRoom,
  Pool,
  RoomService,
  RvHookup,
  SmokeFree,
  SmokingRooms,
  Spa
} as Meta;



const Template: Story<IconProps> = (args) =>
  <>
    <AcUnit {...args} />
    <AirportShuttle {...args} />
    <AllInclusive {...args} />
    <BeachAccess {...args} />
    <BusinessCenter {...args} />
    <Casino {...args} />
    <ChildCare {...args} />
    <ChildFriendly {...args} />
    <FitnessCenter {...args} />
    <FreeBreakfast {...args} />
    <GolfCourse {...args} />
    <HotTub {...args} />
    <Kitchen {...args} />
    <MeetingRoom {...args} />
    <NoMeetingRoom {...args} />
    <Pool {...args} />
    <RoomService {...args} />
    <RvHookup {...args} />
    <SmokeFree {...args} />
    <SmokingRooms {...args} />
    <Spa {...args} />
  </>

export const PlacesIcons = Template.bind({});
PlacesIcons.args = {
  fillColor: 'red',
};



