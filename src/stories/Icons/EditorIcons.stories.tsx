import React from 'react';
import { Story, Meta } from '@storybook/react/types-6-0';
import { IconProps } from './Icons.interface';
import {
  AddComment,
  AttachFile,
  AttachMoney,
  BarChart,
  BorderAll,
  BorderBottom,
  BorderClear,
  BorderColor,
  BorderHorizontal,
  BorderInner,
  BorderLeft,
  BorderOuter,
  BorderRight,
  BorderStyle,
  BorderTop,
  BorderVertical,
  BubbleChart,
  DragHandle,
  FormatAlignCenter,
  FormatAlignJustify,
  FormatAlignLeft,
  FormatAlignRight,
  FormatBold,
  FormatClear,
  FormatColorFill,
  FormatColorReset,
  FormatIndentDecrease,
  FormatIndentIncrease,
  FormatItaliconEditor,
  FormatLineSpacing,
  FormatListBulleted,
  FormatListNumbered,
  FormatListNumberedRtl,
  FormatPaint,
  FormatQuote,
  FormatShapes,
  FormatSize,
  FormatStrikethrough,
  FormatTextdirectionLToR,
  FormatTextdirectionRToL,
  FormatUnderlined,
  Functions,
  Highlight,
  InsertChart,
  InsertChartOutlined,
  InsertComment,
  InsertDriveFile,
  InsertEmoticon,
  InsertInvitation,
  InsertLink,
  InsertPhoto,
  LinearScale,
  MergeType,
  Mode,
  ModeComment,
  MonetizationOn,
  MoneyOff,
  MoneyOffCsred,
  MultilineChart,
  Notes,
  PieChart,
  PieChartOutline,
  Publish,
  ScatterPlot,
  Score,
  ShortText,
  ShowChart,
  SpaceBar,
  StrikethroughS,
  TableChart,
  TextFields,
  Title,
  VerticalAlignBottom,
  VerticalAlignCenter,
  VerticalAlignTop,
  WrapText
} from '@connorfm/amiltone-icons-editor'


export default {
  title: 'Icons/EditorIcons',
  component: AddComment,
  AttachFile,
  AttachMoney,
  BarChart,
  BorderAll,
  BorderBottom,
  BorderClear,
  BorderColor,
  BorderHorizontal,
  BorderInner,
  BorderLeft,
  BorderOuter,
  BorderRight,
  BorderStyle,
  BorderTop,
  BorderVertical,
  BubbleChart,
  DragHandle,
  FormatAlignCenter,
  FormatAlignJustify,
  FormatAlignLeft,
  FormatAlignRight,
  FormatBold,
  FormatClear,
  FormatColorFill,
  FormatColorReset,
  FormatIndentDecrease,
  FormatIndentIncrease,
  FormatItaliconEditor,
  FormatLineSpacing,
  FormatListBulleted,
  FormatListNumbered,
  FormatListNumberedRtl,
  FormatPaint,
  FormatQuote,
  FormatShapes,
  FormatSize,
  FormatStrikethrough,
  FormatTextdirectionLToR,
  FormatTextdirectionRToL,
  FormatUnderlined,
  Functions,
  Highlight,
  InsertChart,
  InsertChartOutlined,
  InsertComment,
  InsertDriveFile,
  InsertEmoticon,
  InsertInvitation,
  InsertLink,
  InsertPhoto,
  LinearScale,
  MergeType,
  Mode,
  ModeComment,
  MonetizationOn,
  MoneyOff,
  MoneyOffCsred,
  MultilineChart,
  Notes,
  PieChart,
  PieChartOutline,
  Publish,
  ScatterPlot,
  Score,
  ShortText,
  ShowChart,
  SpaceBar,
  StrikethroughS,
  TableChart,
  TextFields,
  Title,
  VerticalAlignBottom,
  VerticalAlignCenter,
  VerticalAlignTop,
  WrapText
} as Meta;



const TemplateAB: Story<IconProps> = (args) =>
  <>
    < AddComment {...args} />
    < AttachFile {...args} />
    < AttachMoney {...args} />
    < BarChart {...args} />
    < BorderAll {...args} />
    < BorderBottom {...args} />
    < BorderClear {...args} />
    < BorderColor {...args} />
    < BorderHorizontal {...args} />
    < BorderInner {...args} />
    < BorderLeft {...args} />
    < BorderOuter {...args} />
    < BorderRight {...args} />
    < BorderStyle {...args} />
    < BorderTop {...args} />
    < BorderVertical {...args} />
    < BubbleChart {...args} />
  </>

const TemplateDF: Story<IconProps> = (args) =>
  <>
    < DragHandle {...args} />
    < FormatAlignCenter {...args} />
    < FormatAlignJustify {...args} />
    < FormatAlignLeft {...args} />
    < FormatAlignRight {...args} />
    < FormatBold {...args} />
    < FormatClear {...args} />
    < FormatColorFill {...args} />
    < FormatColorReset {...args} />
    < FormatIndentDecrease {...args} />
    < FormatIndentIncrease {...args} />
    < FormatItaliconEditor {...args} />
    < FormatLineSpacing {...args} />
    < FormatListBulleted {...args} />
    < FormatListNumbered {...args} />
    < FormatListNumberedRtl {...args} />
    < FormatPaint {...args} />
    < FormatQuote {...args} />
    < FormatShapes {...args} />
    < FormatSize {...args} />
    < FormatStrikethrough {...args} />
    < FormatTextdirectionLToR {...args} />
    < FormatTextdirectionRToL {...args} />
    < FormatUnderlined {...args} />
    < Functions {...args} />
  </>

const TemplateHN: Story<IconProps> = (args) =>
  <>
    < Highlight {...args} />
    < InsertChart {...args} />
    < InsertChartOutlined {...args} />
    < InsertComment {...args} />
    < InsertDriveFile {...args} />
    < InsertEmoticon {...args} />
    < InsertInvitation {...args} />
    < InsertLink {...args} />
    < InsertPhoto {...args} />
    < LinearScale {...args} />
    < MergeType {...args} />
    < Mode {...args} />
    < ModeComment {...args} />
    < MonetizationOn {...args} />
    < MoneyOff {...args} />
    < MoneyOffCsred {...args} />
    < MultilineChart {...args} />
    < Notes {...args} />
  </>
const TemplatePW: Story<IconProps> = (args) =>
  <>
    < PieChart {...args} />
    < PieChartOutline {...args} />
    < Publish {...args} />
    < ScatterPlot {...args} />
    < Score {...args} />
    < ShortText {...args} />
    < ShowChart {...args} />
    < SpaceBar {...args} />
    < StrikethroughS {...args} />
    < TableChart {...args} />
    < TextFields {...args} />
    < Title {...args} />
    < VerticalAlignBottom {...args} />
    < VerticalAlignCenter {...args} />
    < VerticalAlignTop {...args} />
    < WrapText {...args} />
  </>


export const EditorIconsAB = TemplateAB.bind({});
EditorIconsAB.args = {
  fillColor: 'red',
};
export const EditorIconsDF = TemplateDF.bind({});
EditorIconsDF.args = {
  fillColor: 'red',
};
export const EditorIconsHN = TemplateHN.bind({});
EditorIconsHN.args = {
  fillColor: 'red',
};
export const EditorIconsPW = TemplatePW.bind({});
EditorIconsPW.args = {
  fillColor: 'red',
};




