import React from 'react';
import { Story, Meta } from '@storybook/react/types-6-0';
import { IconProps } from './Icons.interface';
import {
  AddToQueue,
  Airplay,
  Album,
  ArtTrack,
  BrandingWatermark,
  // CallToAv,
  ClosedCaption,
  ControlCamera,
  Equalizer,
  Explicit,
  FastForward,
  FastRewind,
  FeaturedPlayList,
  FeaturedVideo,
  FiberDvr,
  FiberManualRecord,
  FiberNew,
  FiberPin,
  FiberSmartRecord,
  Forward10,
  Forward30,
  Forward5,
  FourK,
  Games,
  Hd,
  Hearing,
  HighQuality,
  LibraryAdd,
  LibraryBooks,
  LibraryMusic,
  Loop,
  Mic,
  MicNone,
  MicOff,
  MissedVideoCall,
  Movie,
  MusicVideo,
  NewReleases,
  NotInterested,
  Note,
  Pause,
  PauseCircleFilled,
  PauseCircleOutline,
  PlayArrow,
  PlayCircleFilled,
  PlayCircleFilledWhite,
  PlayCircleOutline,
  PlaylistAdd,
  PlaylistAddCheck,
  PlaylistPlay,
  Queue,
  QueueMusicon,
  QueuePlayNext,
  Radio,
  RecentActors,
  RemoveFromQueue,
  Repeat,
  RepeatOne,
  Replay,
  Replay10,
  Replay30,
  Replay5,
  Shuffle,
  SkipNext,
  SkipPrevious,
  SlowMotionVideo,
  Snooze,
  SortByAlpha,
  Stop,
  Subscriptions,
  Subtitles,
  SurroundSound,
  Timer,
  VideoCall,
  VideoLabel,
  VideoLibrary,
  Videocam,
  VideocamOff,
  VolumeDown,
  VolumeMute,
  VolumeOff,
  VolumeUp,
  Web,
  WebAsset
}
  from '@connorfm/amiltone-icons-av'

export default {
  title: 'Icons/AvIcons',
  component: AddToQueue,
  Airplay,
  Album,
  ArtTrack,
  BrandingWatermark,
  // CallToAv,
  ClosedCaption,
  ControlCamera,
  Equalizer,
  Explicit,
  FastForward,
  FastRewind,
  FeaturedPlayList,
  FeaturedVideo,
  FiberDvr,
  FiberManualRecord,
  FiberNew,
  FiberPin,
  FiberSmartRecord,
  Forward10,
  Forward30,
  Forward5,
  FourK,
  Games,
  Hd,
  Hearing,
  HighQuality,
  LibraryAdd,
  LibraryBooks,
  LibraryMusic,
  Loop,
  Mic,
  MicNone,
  MicOff,
  MissedVideoCall,
  Movie,
  MusicVideo,
  NewReleases,
  NotInterested,
  Note,
  Pause,
  PauseCircleFilled,
  PauseCircleOutline,
  PlayArrow,
  PlayCircleFilled,
  PlayCircleFilledWhite,
  PlayCircleOutline,
  PlaylistAdd,
  PlaylistAddCheck,
  PlaylistPlay,
  Queue,
  QueueMusicon,
  QueuePlayNext,
  Radio,
  RecentActors,
  RemoveFromQueue,
  Repeat,
  RepeatOne,
  Replay,
  Replay10,
  Replay30,
  Replay5,
  Shuffle,
  SkipNext,
  SkipPrevious,
  SlowMotionVideo,
  Snooze,
  SortByAlpha,
  Stop,
  Subscriptions,
  Subtitles,
  SurroundSound,
  Timer,
  VideoCall,
  VideoLabel,
  VideoLibrary,
  Videocam,
  VideocamOff,
  VolumeDown,
  VolumeMute,
  VolumeOff,
  VolumeUp,
  Web,
  WebAsset
} as Meta;



const TemplateA: Story<IconProps> = (args) =>
  <>
    <AddToQueue {...args} />
    <Airplay {...args} />
    <Album {...args} />
    <ArtTrack {...args} />
  </>;
const TemplateB: Story<IconProps> = (args) =>
  <BrandingWatermark {...args} />;
const TemplateC: Story<IconProps> = (args) =>
  <>
    {/* <CallToAv {...args} /> */}
    <ClosedCaption {...args} />
    <ControlCamera {...args} />
  </>;
const TemplateE: Story<IconProps> = (args) =>
  <>
    <Equalizer {...args} />
    <Explicit {...args} />
  </>
const TemplateF: Story<IconProps> = (args) =>
  <>
    <FastForward {...args} />
    <FastRewind {...args} />
    <FeaturedPlayList {...args} />
    <FeaturedVideo {...args} />
    <FiberDvr {...args} />
    <FiberManualRecord {...args} />
    <FiberNew {...args} />
    <FiberPin {...args} />
    <FiberSmartRecord {...args} />
    <Forward10 {...args} />
    <Forward30 {...args} />
    <Forward5 {...args} />
    <FourK {...args} />
  </>
const TemplateG: Story<IconProps> = (args) =>
  <Games {...args} />
const TemplateH: Story<IconProps> = (args) =>
  <>
    <Hd {...args} />
    <Hearing {...args} />
    <HighQuality {...args} />
  </>
const TemplateL: Story<IconProps> = (args) =>
  <>
    <LibraryAdd {...args} />
    <LibraryBooks {...args} />
    <LibraryMusic {...args} />
    <Loop {...args} />
  </>
const TemplateM: Story<IconProps> = (args) =>
  <>
    <Mic {...args} />
    <MicNone {...args} />
    <MicOff {...args} />
    <MissedVideoCall {...args} />
    <Movie {...args} />
    <MusicVideo {...args} />
  </>
const TemplateN: Story<IconProps> = (args) =>
  <>
    <NewReleases {...args} />
    <NotInterested {...args} />
    <Note {...args} />
  </>
const TemplateP: Story<IconProps> = (args) =>
  <>
    <Pause {...args} />
    <PauseCircleFilled {...args} />
    <PauseCircleOutline {...args} />
    <PlayArrow {...args} />
    <PlayCircleFilled {...args} />
    <PlayCircleFilledWhite {...args} />
    <PlayCircleOutline {...args} />
    <PlaylistAdd {...args} />
    <PlaylistAddCheck {...args} />
    <PlaylistPlay {...args} />
  </>
const TemplateQ: Story<IconProps> = (args) =>
  <>
    <Queue {...args} />
    <QueueMusicon {...args} />
    <QueuePlayNext {...args} />
  </>
const TemplateR: Story<IconProps> = (args) =>
  <>
    <Radio {...args} />
    <RecentActors {...args} />
    <RemoveFromQueue {...args} />
    <Repeat {...args} />
    <RepeatOne {...args} />
    <Replay {...args} />
    <Replay10 {...args} />
    <Replay30 {...args} />
    <Replay5 {...args} />
  </>
const TemplateS: Story<IconProps> = (args) =>
  <>
    <Shuffle {...args} />
    <SkipNext {...args} />
    <SkipPrevious {...args} />
    <SlowMotionVideo {...args} />
    <Snooze {...args} />
    <SortByAlpha {...args} />
    <Stop {...args} />
    <Subscriptions {...args} />
    <Subtitles {...args} />
    <SurroundSound {...args} />
  </>
const TemplateT: Story<IconProps> = (args) =>
  <Timer {...args} />
const TemplateV: Story<IconProps> = (args) =>
  <>
    <VideoCall {...args} />
    <VideoLabel {...args} />
    <VideoLibrary {...args} />
    <Videocam {...args} />
    <VideocamOff {...args} />
    <VolumeDown {...args} />
    <VolumeMute {...args} />
    <VolumeOff {...args} />
    <VolumeUp {...args} />
  </>
const TemplateW: Story<IconProps> = (args) =>
  <>
    <Web {...args} />
    <WebAsset {...args} />
  </>;


export const AvIconsA = TemplateA.bind({});
AvIconsA.args = {
  fillColor: 'red',
};
export const AvIconsB = TemplateB.bind({});
AvIconsB.args = {
  fillColor: 'red',
};
export const AvIconsC = TemplateC.bind({});
AvIconsC.args = {
  fillColor: 'red',
};
export const AvIconsE = TemplateE.bind({});
AvIconsE.args = {
  fillColor: 'red',
};
export const AvIconsF = TemplateF.bind({});
AvIconsF.args = {
  fillColor: 'red',
};
export const AvIconsG = TemplateG.bind({});
AvIconsG.args = {
  fillColor: 'red',
};
export const AvIconsH = TemplateH.bind({});
AvIconsH.args = {
  fillColor: 'red',
};
export const AvIconsL = TemplateL.bind({});
AvIconsL.args = {
  fillColor: 'red',
};
export const AvIconsM = TemplateM.bind({});
AvIconsM.args = {
  fillColor: 'red',
};
export const AvIconsN = TemplateN.bind({});
AvIconsN.args = {
  fillColor: 'red',
};
export const AvIconsP = TemplateP.bind({});
AvIconsP.args = {
  fillColor: 'red',
};
export const AvIconsQ = TemplateQ.bind({});
AvIconsQ.args = {
  fillColor: 'red',
};
export const AvIconsR = TemplateR.bind({});
AvIconsR.args = {
  fillColor: 'red',
};
export const AvIconsS = TemplateS.bind({});
AvIconsS.args = {
  fillColor: 'red',
};
export const AvIconsT = TemplateT.bind({});
AvIconsT.args = {
  fillColor: 'red',
};
export const AvIconsV = TemplateV.bind({});
AvIconsV.args = {
  fillColor: 'red',
};
export const AvIconsW = TemplateW.bind({});
AvIconsW.args = {
  fillColor: 'red',
};



