import React from 'react';
import styled from 'styled-components';

export interface RadioProps {
  /**
   * id
   */
  id: string;
  /**
   * Button label
   */
  label: string;
  /**
   * Button value
   */
  value?: string;
  /**
   * Is the button enabled ?
   */
  disabled?: boolean;
  /**
  * Is the button checked ?
  */
  checked?: boolean;/**
  * Default checked ?
  */
  defaultChecked?: boolean;
  /**
   * Optional click handler
   */
  onChange?: (e: React.FormEvent<HTMLInputElement>) => any;
  /**
   * radio color
   */
  color?: string;
  /**
   * label Color
   */
  textColor?: string;
}


const RadioInput = styled.input`
  &:checked{
    & + label {
      &:before {
        border: 2px solid ${props => props.color};
      }
      &:after {
        background: ${props => props.color};
      }
    }
  }
  &:not(:checked){
    & + label {
      &:before {
        border: 2px solid #dadada;
      }
      &:after {
        background: #dadada;
      }
    }
  }
  &:checked,
  &:not(:checked) {
    position: absolute;
    left: -9999px;
    & + label {
      position: relative;
      padding-left: 28px;
      cursor: pointer;
      line-height: 20px;
      display: inline-block;
      color: #666;
      margin-bottom: 5px;
      &:before{
        content: '';
        position: absolute;
        left: 0;
        top: 0;
        width: 18px;
        height: 18px;
        border-radius: 100%;
        background: transparent;
      }
      &:after{
        content: '';
        width: 10px;
        height: 10px;
        position: absolute;
        top: 6px;
        left: 6px;
        border-radius: 100%;
        -webkit-transition: all 0.2s ease;
        transition: all 0.2s ease;
      }
    }
  &:checked:disabled,
  &:not(:checked):disabled {
    & + label {
      color: #666;
      &:before{
        border: 2px solid #dadada;
        background: transparent;
      }
      &:after{
        height: 10px;
        background: #dadada;
      }
    }
  }
  &:not(:checked) + label:after {
    opacity: 0;
    -webkit-transform: scale(0);
    transform: scale(0);
  }
  &:checked + label:after {
      opacity: 1;
      -webkit-transform: scale(1);
      transform: scale(1);
  }
}
`


/**
 * Primary UI component for user interaction
 */
export const Radio: React.FC<RadioProps> = ({
  id,
  label,
  value,
  disabled,
  checked,
  defaultChecked,
  color = '#7F3EFA',
  textColor = 'black',
  ...props
}) => {
  if (!value) {
    value = label
  }
  return (
    <div>
      <RadioInput type="radio" id={id} name="radio-group" value={value} disabled={disabled} checked={checked} color={color} defaultChecked={defaultChecked} />
      <label htmlFor={id}>{label}</label>
    </div>
  )
}


