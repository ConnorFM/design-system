import React from 'react';
import { Story, Meta } from '@storybook/react/types-6-0';

import { Radio, RadioProps } from './Radio';

export default {
  title: 'SelectionControl/Radio',
  component: Radio,

} as Meta;

const Template: Story<RadioProps> = (args) => <Radio {...args} />;

export const Checked = Template.bind({});
Checked.args = {
  label: 'Radio',
  id: 'id',
  value: 'value',
  checked: true
}

export const Unchecked = Template.bind({});
Unchecked.args = {
  label: 'Radio',
  id: 'id',
  value: 'value',
  checked: false
}

export const Disabled = Template.bind({});
Disabled.args = {
  label: 'Radio',
  id: 'id',
  value: 'value',
  checked: true,
  disabled: true
}

export const Multiple: Story<RadioProps> = () => (
  <>
    <Radio label='Choice 1' id='1' />
    <Radio label='Choice 2' id='2' />
    <Radio label='Choice 3' id='3' defaultChecked />
  </>
)

