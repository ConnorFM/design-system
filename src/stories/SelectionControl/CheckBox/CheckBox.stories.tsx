import React from 'react';
import { Story, Meta } from '@storybook/react/types-6-0';

import { CheckBox, CheckBoxProps } from './CheckBox';

export default {
  title: 'SelectionControl/CheckBox',
  component: CheckBox,

} as Meta;

const Template: Story<CheckBoxProps> = (args) => <CheckBox {...args} />;

export const Checked = Template.bind({});
Checked.args = {
  checked: true,
  checkType: 'check'
}

export const CheckedLabeled = Template.bind({});
CheckedLabeled.args = {
  checked: true,
  checkType: 'check',
  labeled: true,
  label: 'Your label here'
}

export const CheckedDashed = Template.bind({});
CheckedDashed.args = {
  checked: true,
  checkType: 'dash'
}

export const Unchecked = Template.bind({});
Unchecked.args = {
  defaultChecked: false
}

export const Disabled = Template.bind({});
Disabled.args = {
  disabled: true
}

export const CheckedDisabled = Template.bind({});
CheckedDisabled.args = {
  disabled: true,
  checked: true
}
