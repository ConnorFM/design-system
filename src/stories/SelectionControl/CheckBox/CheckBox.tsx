import React from 'react';
import styled from 'styled-components';
import { primaryColor } from '../../assets/StyleConstants/index';

export interface CheckBoxProps {
  /**
   * labeled ?
   */
  labeled?: boolean;
  /**
   * label
   */
  label?: string;
  /**
   * id
   */
  id?: string;
  /**
   * value
   */
  value?: string;
  /**
   * Is the button enabled ?
   */
  disabled?: boolean;
  /**
  * Is the button checked ?
  */
  checked?: boolean;
  /**
  * Default checked ?
  */
  defaultChecked?: boolean;
  /**
   * Optional click handler
   */
  onChange?: (e: React.FormEvent<HTMLInputElement>) => any;
  /**
   * background color
   */
  backgroundColor?: string;
  /**
   * check color , # for Hex colors must be replaced with %23
   */
  checkColor?: string;
  /**
   * unchecked border color
   */
  uncheckedColor?: string;
  /**
   * type of check
   */
  checkType?: 'check' | 'dash'
  /**
   * path,autofilled
   */
  path?: string
  /**
   * path position,autofilled
   */
  pathPosition?: string
}


const Box = styled.div`
display: flex;
  align-items: center;
  user-select: none;
  & label {
    font-size: 16px;
    color: #4D4D4D;
    position: absolute;
    z-index: 10;
    padding-left: 28px;
    cursor: pointer;
  }
  & input {
    opacity: 0;
    visibility: hidden;
    position: absolute;
    &:checked ~ .check {
      border-color: ${(props: CheckBoxProps) => props.backgroundColor};
      box-shadow: 0px 0px 0px 15px ${(props: CheckBoxProps) => props.backgroundColor} inset;
      &::after {
        opacity: 1;
        transform: scale(1);
      }
    }
    &:disabled:checked ~ .check{
      border-color: ${(props: CheckBoxProps) => props.uncheckedColor};
      box-shadow: 0px 0px 0px 15px ${(props: CheckBoxProps) => props.uncheckedColor} inset;
    }
  }
  & .check {
    width: 18px;
    height: 18px;
    border-radius: 3px;
    display: flex;
    justify-content: center;
    align-items: center;
    position: relative;
    background-color: #FFF;
    border: 2px solid ${(props: CheckBoxProps) => props.uncheckedColor};
    box-shadow: 0px 0px 0px 0px ${(props: CheckBoxProps) => props.backgroundColor} inset;
    transition: all 0.15s cubic-bezier(0, 1.05, 0.72, 1.07);
    &::after {
      content: '';
      width: 100%;
      height: 100%;
      opacity: 0;
      z-index: 4;
      position: absolute;
      transform: scale(0);
      background-size: 100%;
      background-image: url("data:image/svg+xml;utf8,<svg width='18' height='14' viewBox='0 0 18 14' fill='none' xmlns='http://www.w3.org/2000/svg'><path d='${(props: CheckBoxProps) => props.path}' fill='${(props: CheckBoxProps) => props.checkColor}' /></svg>");
      background-repeat: no-repeat;
      background-position: ${(props: CheckBoxProps) => props.pathPosition};
      transition-delay: 0.2s !important;
      transition: all 0.25s cubic-bezier(0, 1.05, 0.72, 1.07);
    }
  }
`

/**
 * Primary UI component for user interaction
 */
export const CheckBox: React.FC<CheckBoxProps> = ({
  labeled,
  label,
  id,
  value,
  disabled,
  checked,
  defaultChecked,
  backgroundColor = primaryColor,
  checkColor = '%23fff',
  uncheckedColor = 'rgba(218,218,218,1)',
  checkType,
  path,
  pathPosition,
  ...props
}) => {
  if (checkType === 'check') {
    path = 'M5.9999 11.2L1.7999 7L0.399902 8.4L5.9999 14L17.9999 2L16.5999 0.599998L5.9999 11.2Z';
    pathPosition = 'center';
  } else {
    path = 'M16 2H2V5H16V0Z'
    pathPosition = '50% 120%'
  };
  return (
    <Box backgroundColor={backgroundColor} checkColor={checkColor} uncheckedColor={uncheckedColor} path={path} pathPosition={pathPosition} checkType={checkType}>
      <input id={id} type="checkbox" value={value} disabled={disabled} checked={checked} defaultChecked={defaultChecked} />
      <span className="check"></span>
      {labeled ? <label htmlFor={id}>{label}</label> : void 0}
    </Box>
  )
}


