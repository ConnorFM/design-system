import React from 'react';
import { Story, Meta } from '@storybook/react/types-6-0';

import { ToggleSwitch, ToggleProps } from './ToggleSwitch';

export default {
  title: 'SelectionControl/ToggleSwitch',
  component: ToggleSwitch,

} as Meta;

const Template: Story<ToggleProps> = (args) => <ToggleSwitch {...args} />;

export const Checked = Template.bind({});
Checked.args = {
  checked: true,
}

export const Unchecked = Template.bind({});
Unchecked.args = {
  defaultChecked: false
}

export const Disabled = Template.bind({});
Disabled.args = {
  disabled: true
}
