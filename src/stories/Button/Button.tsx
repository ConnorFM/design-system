import React from 'react';
import styled from 'styled-components';

export interface ButtonProps {
  /**
   * Is this the principal call to action on the page?
   */
  filled?: boolean;
  /**
   * Is the button outlined
   */
  outlined?: boolean;
  /**
   * What label color to use
   */
  color?: string;/**
   * What background color to use
   */
  backgroundColor?: string;
  /**
   * How large should the button be?
   */
  size?: 'tiny' | 'small' | 'medium' | 'large' | 'giant';
  /**
   * Button contents
   */
  label?: string;
  /**
   * Does the button have icon ?
   */
  iconned?: 'double' | 'right' | 'left' | 'single' | 'none',
  /**
  * icon path
  */
  icon?: JSX.Element,
  /**
  * inline SVG  ?
  */
  iconColor?: string,
  /**
   * Optional click handler
   */
  onClick?: () => any;
}


const ButtonFrame = styled.div<ButtonProps>`
  font-family: 'Nunito Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
  font-weight: 600;
  border: 0;
  border-radius: 6px;
  cursor: pointer;
  display: inline-block;
  line-height: 1;
  display: flex;
  align-items: center;
  width: fit-content;
  text-transform: uppercase;
  background-color: ${(props: ButtonProps) => props.filled && !props.outlined ? props.backgroundColor : 'transparent'};
  color:  ${(props: ButtonProps) => props.color};
  box-shadow: ${(props: ButtonProps) => !props.color && !props.outlined ? 'rgba(0, 0, 0, 0.15) 0px 0px 0px 1px inset' : void 0};
  font-size: ${(props: ButtonProps) => props.size === 'tiny' || props.size === 'small' ? '12px'
    : props.size === 'medium' ? '14px'
      : props.size === 'large' ? '16px'
        : '18px'};
  padding: ${(props: ButtonProps) => props.size === 'tiny' ? '8px 11px'
    : props.size === 'small' ? '10px 13px'
      : props.size === 'medium' ? '10px 18px'
        : props.size === 'large' ? '12px 24px'
          : '16px 26px'};
  .icon-text{
    padding: ${(props: ButtonProps) => props.size === 'tiny' ? '0px 7px'
    : props.size === 'small' ? '0px 9px'
      : props.size === 'medium' ? '0px 10px'
        : props.size === 'large' ? '0px 10px'
          : '0px 12px'};
  }
  .right-icon-text{
    padding: ${(props: ButtonProps) => props.size === 'tiny' ? '0 7px 0 0'
    : props.size === 'small' ? '0 9px 0 0'
      : props.size === 'medium' ? '0 10px 0 0'
        : props.size === 'large' ? '0 10px 0 0'
          : '0 12px 0 0'};
  }
  .left-icon-text{
    padding: ${(props: ButtonProps) => props.size === 'tiny' ? '0 0 0 7px'
    : props.size === 'small' ? '0 0 0 9px'
      : props.size === 'medium' ? '0 0 0 10px'
        : props.size === 'large' ? '0 0 0 10px'
          : '0 0 0 12px'};
  }
  .no-icon-text {
    padding: 0
  }
  svg{
    height: ${(props: ButtonProps) => props.size === 'tiny' ? '13px'
    : props.size === 'small' ? '13px'
      : props.size === 'medium' ? '19px'
        : props.size === 'large' ? '19px'
          : '19px'};
  }
`

export const Button: React.FC<ButtonProps> = ({
  filled = false,
  outlined = false,
  size = 'medium',
  color,
  backgroundColor,
  label,
  iconned = 'none',
  icon,
  iconColor,
  ...props
}) => {
  const leftIcon = iconned === 'double' || iconned === 'left' ? icon : void 0;
  const rightIcon = iconned === 'double' || iconned === 'right' ? icon : void 0;
  const singleIcon = iconned === 'single' ? icon : void 0;
  let labelClass = '';
  if (iconned === 'none') {
    labelClass = 'no-icon-text';
  } else if (iconned === 'right') {
    labelClass = 'right-icon-text';
  } else if (iconned === 'left') {
    labelClass = 'left-icon-text';
  } else if (iconned === 'double') {
    labelClass = 'icon-text';
  } else if (iconned === 'single') {
    label = ''
  }


  return (
    <ButtonFrame backgroundColor={backgroundColor} filled={filled} color={color} outlined={outlined} size={size}>
      {leftIcon}
      {singleIcon}
      <span className={labelClass}>
        {label}
      </span>
      {rightIcon}
    </ButtonFrame>
  )
}


