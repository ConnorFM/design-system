import React from 'react';
import { Story, Meta } from '@storybook/react/types-6-0';
import { StarRate } from '@connorfm/amiltone-icons-action'

import { Button, ButtonProps } from './Button';

export default {
  title: 'Button/Button',
  component: Button,
} as Meta;

const Template: Story<ButtonProps> = (args) => <Button {...args} />;

export const FilledMedium = Template.bind({});
FilledMedium.args = {
  filled: true,
  label: 'Button',
};

export const SecondaryMedium = Template.bind({});
SecondaryMedium.args = {
  label: 'Button',
};

export const Outlined = Template.bind({});
Outlined.args = {
  label: 'Button',
  outlined: true
};

export const RightStarred = Template.bind({});
RightStarred.args = {
  filled: true,
  label: 'Button',
  color: '#fff',
  iconned: 'right',
  icon: <StarRate fillColor='#fff' />
};

console.log();


export const LeftStarred = Template.bind({});
LeftStarred.args = {
  label: 'Button',
  filled: true,
  color: '#fff',
  iconned: 'left',
  icon: <StarRate fillColor='#fff' />
};

export const DoubleStarred = Template.bind({});
DoubleStarred.args = {
  label: 'Button',
  filled: true,
  color: '#fff',
  iconned: 'double',
  icon: <StarRate fillColor='#fff' />
};

export const Giant = Template.bind({});
Giant.args = {
  size: 'giant',
  label: 'Button',
};

export const Large = Template.bind({});
Large.args = {
  size: 'large',
  label: 'Button',
};

export const Small = Template.bind({});
Small.args = {
  size: 'small',
  label: 'Button',
};

export const Tiny = Template.bind({});
Tiny.args = {
  size: 'tiny',
  label: 'Button',
};
